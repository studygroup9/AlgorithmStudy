package test;

import java.util.ArrayList;

public class W23_Q4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		solution(new int[] {1,1,3,3,0,1,1});
	}
	public static ArrayList<Integer> solution(int []arr) {
        ArrayList<Integer> answer = new ArrayList<Integer>();
	    int j = 0;
	    
	    for(int i=0; i < arr.length; i++) {
	    	if(i == 0) {
	    		answer.add(arr[i]);
	    		j++;
	    	}else {
	    		if(answer.get(j-1) != arr[i]){
	    			answer.add(arr[i]);
	    			j++;
	    		}
	    	}
	    }
	    
	    
	    return answer;
    }
}
