package test;

public class W23_Q2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		lengthOfLongestSubstring("abcabcbb");
	}
	public static int lengthOfLongestSubstring(String s) {
        int max = -1;
		StringBuilder sb = new StringBuilder();
		
		if(s.equals(""))
			return 0;
		
		for(int i=0; i <= s.length()-1; i++) {
			
			if(sb.indexOf(s.substring(i, i+1)) == -1) {
				sb.append(s.substring(i, i+1));
				max = Math.max(sb.length(), max);
				
			}else {
				sb.delete(0, sb.indexOf(s.substring(i, i+1))+1);
				sb.append(s.substring(i, i+1));
			}
			// System.out.println(i + ": " + sb.indexOf(s.substring(i, i+1)) 
			// + " : " + s.substring(i, i+1) + " : " + sb.length() + " : " + sb);
		}
		
		System.out.println(max);
		
		return max;
    }
}
