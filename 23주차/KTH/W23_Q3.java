package test;

import java.util.Arrays;

public class W23_Q3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		solution(5,new int[] {9, 20, 28, 18, 11}, new int[] {30, 1, 21, 17, 28});
	}
	public static String[] solution(int n, int[] arr1, int[] arr2) {
       String[] answer = new String[arr1.length];
        
        for(int i=0; i < n; i++) {
        	int res = arr1[i] | arr2[i];
        	answer[i] = String.format("%"+n+"s", Integer.toBinaryString(res)).replaceAll("0", " ").replaceAll("1", "#");
        }
        
        System.out.println(Arrays.toString(answer));
        
        //"#####","# # #", "### #", "# ##", "#####"
        return answer;
    }
}
