// 가장 먼 노드
// https://programmers.co.kr/learn/courses/30/lessons/49189

// 구해야 하는 답: 가장 멀리 떨어진 노드가 몇개인지
// 답을 구하기 위해.. 가장 멀리 떨어진 것 (depth의 정도) 알아야 하고, 거기에 해당하는 노드 개수 알아야 함

function solution(n, edge) {
  const graph = new Array(n).fill(null).map(() => []);
  // 이차원 배열 만들기

  edge.forEach(([a, b]) => {
    // 양방향이므로 양쪽에 넣어주기 (노드모양대로 탐색하기 위해 만드는 과정)
    // 노드에는 1부터 들어가지만, 배열 인덱스에 맞추기 위해서 모두 -1 처리
    graph[a - 1].push(b - 1);
    graph[b - 1].push(a - 1);
  });

  // console.log('graph: ', graph);

  const queue = [[0, 0]];
  const visited = [true];
  const depNum = []; // 해당하는 깊이에 존재하는 노드의 개수를 기록

  while (queue.length) {
    const [cur, dep] = queue.shift();
    // 뽑은 인자는 배열. 그 첫번째 값은 cur 로, 두번째 값은 dep으로 쓰도록 함

    depNum[dep] = depNum[dep] ? depNum[dep] + 1 : 1;
    // dep에 해당하는 노드의 개수를 depNum에 기록함

    graph[cur].forEach((n) => {
      if (!visited[n]) {
        // 방문한 노드는 또 방문하지 않음

        visited[n] = true;
        queue.push([n, dep + 1]);
      }
    });
  }

  // console.log(depNum);
  return depNum[depNum.length - 1];
}

/**
테스트 1 〉	통과 (0.18ms, 30.2MB)
테스트 2 〉	통과 (0.21ms, 30MB)
테스트 3 〉	통과 (0.25ms, 30.3MB)
테스트 4 〉	통과 (0.44ms, 30.4MB)
테스트 5 〉	통과 (1.29ms, 30.7MB)
테스트 6 〉	통과 (4.44ms, 32.8MB)
테스트 7 〉	통과 (20.95ms, 48.6MB)
테스트 8 〉	통과 (23.47ms, 53.5MB)
테스트 9 〉	통과 (43.51ms, 53.9MB)
 */

// 참고: https://m.blog.naver.com/ojhwlxk/222076206668
