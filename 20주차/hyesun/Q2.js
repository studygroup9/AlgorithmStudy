// 부족한 금액 계산하기
// https://programmers.co.kr/learn/courses/30/lessons/82612

function solution(price, money, count) {
  let totalPrice = 0;
  for (let i = 1; i <= count; i++) {
    totalPrice += price * i;
  }

  return money - totalPrice > 0 ? 0 : Math.abs(money - totalPrice);
}

/**
테스트 1 〉	통과 (0.05ms, 30.2MB)
테스트 2 〉	통과 (0.05ms, 30.3MB)
테스트 3 〉	통과 (0.04ms, 30.3MB)
테스트 4 〉	통과 (0.04ms, 30.2MB)
테스트 5 〉	통과 (0.04ms, 30.3MB)
테스트 6 〉	통과 (0.06ms, 30.2MB)
테스트 7 〉	통과 (0.05ms, 30.2MB)
테스트 8 〉	통과 (0.05ms, 30.2MB)
테스트 9 〉	통과 (0.05ms, 30.2MB)
테스트 10 〉	통과 (0.05ms, 30.1MB)
테스트 11 〉	통과 (0.05ms, 30.3MB)
테스트 12 〉	통과 (0.06ms, 30.2MB)
테스트 13 〉	통과 (0.06ms, 30.3MB)
테스트 14 〉	통과 (0.07ms, 30MB)
테스트 15 〉	통과 (0.07ms, 30.3MB)
테스트 16 〉	통과 (0.06ms, 30.2MB)
테스트 17 〉	통과 (0.06ms, 30MB)
테스트 18 〉	통과 (0.09ms, 30.3MB)
테스트 19 〉	통과 (0.12ms, 29.9MB)
테스트 20 〉	통과 (0.10ms, 30.3MB)
테스트 21 〉	통과 (0.10ms, 30.3MB)
테스트 22 〉	통과 (0.09ms, 30.2MB)
테스트 23 〉	통과 (0.06ms, 30.2MB)
 */
