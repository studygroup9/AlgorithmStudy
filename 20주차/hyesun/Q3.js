// 예산
// https://programmers.co.kr/learn/courses/30/lessons/12982

function solution(d, budget) {
  let rest = budget;
  let answer = 0;
  d = [...d.sort((a, b) => a - b)];
  for (const value of d) {
    if (rest - value >= 0) {
      rest -= value;
      answer++;
      continue;
    }
    return answer;
  }

  return answer;
}

/**
테스트 1 〉	통과 (0.06ms, 30.2MB)
테스트 2 〉	통과 (0.06ms, 30.2MB)
테스트 3 〉	통과 (0.06ms, 30.4MB)
테스트 4 〉	통과 (0.07ms, 30.2MB)
테스트 5 〉	통과 (0.07ms, 30.4MB)
테스트 6 〉	통과 (0.07ms, 30.2MB)
테스트 7 〉	통과 (0.09ms, 30.3MB)
테스트 8 〉	통과 (0.09ms, 30.4MB)
테스트 9 〉	통과 (0.09ms, 30.2MB)
테스트 10 〉	통과 (0.09ms, 30.2MB)
테스트 11 〉	통과 (0.10ms, 30.3MB)
테스트 12 〉	통과 (0.10ms, 30.2MB)
테스트 13 〉	통과 (0.09ms, 30.2MB)
테스트 14 〉	통과 (0.17ms, 30.2MB)
테스트 15 〉	통과 (0.09ms, 30MB)
테스트 16 〉	통과 (0.11ms, 30.4MB)
테스트 17 〉	통과 (0.09ms, 30.3MB)
테스트 18 〉	통과 (0.09ms, 30.2MB)
테스트 19 〉	통과 (0.09ms, 30.2MB)
테스트 20 〉	통과 (0.08ms, 30.2MB)
테스트 21 〉	통과 (0.09ms, 30.3MB)
테스트 22 〉	통과 (0.08ms, 30.3MB)
테스트 23 〉	통과 (0.08ms, 30.3MB)
 */
