// 삼각 달팽이
// https://programmers.co.kr/learn/courses/30/lessons/68645

function solution(n) {
  const numArr = [];

  // 0으로 채워진 삼각달팽이의 틀(2차원 배열)을 만들기
  for (let i = 1; i <= n; i++) {
    numArr.push(new Array(i).fill(0));
  }

  let row = -1,
    col = 0;

  // 각 배열에 채울 값
  let curNum = 1;

  for (let i = n; i >= 1; i -= 3) {
    for (let j = 0; j < i; j++) {
      numArr[++row][col] = curNum++;
    }

    for (let j = 0; j < i - 1; j++) {
      numArr[row][++col] = curNum++;
    }

    for (let j = 0; j < i - 2; j++) {
      numArr[--row][--col] = curNum++;
    }
  }

  // console.log('numArr: ', numArr);
  return numArr.flat();
}

/*
테스트 1 〉	통과 (0.11ms, 30MB)
테스트 2 〉	통과 (0.10ms, 30.2MB)
테스트 3 〉	통과 (0.12ms, 30.1MB)
테스트 4 〉	통과 (2.13ms, 33.2MB)
테스트 5 〉	통과 (2.15ms, 33.5MB)
테스트 6 〉	통과 (1.05ms, 33.3MB)
테스트 7 〉	통과 (127.74ms, 77.6MB)
테스트 8 〉	통과 (89.81ms, 84.2MB)
테스트 9 〉	통과 (139.46ms, 84.1MB)
*/
