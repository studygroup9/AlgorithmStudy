package test;

import java.util.Arrays;

public class W20_Q5 {

	/*
	 * 정수 n이 매개변수로 주어집니다. 
	 * 다음 그림과 같이 밑변의 길이와 높이가 n인 삼각형에서 맨 위 꼭짓점부터 반시계 방향으로 달팽이 채우기를 진행한 후, 
	 * 첫 행부터 마지막 행까지 모두 순서대로 합친 새로운 배열을 return 하도록 solution 함수를 완성해주세요.
	 * 
	 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		solution(4);
	}
	public static int[] solution(int n) {
        int[] answer = new int[n*(n+1)/2];
        int[][] tr = new int [n][n];
        int tmp = 0;
        
        int x = 0, y = -1;
        int num = 1;

        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
            	// System.out.println(i%3 + ", j= " + j);
            	
            	if (i % 3 == 0) {   //배열 세로좌표
                    y++;
                } else if (i % 3 == 1) {    //배열 가로좌표
                    x++;
                } else if (i % 3 == 2) {    //대각선 좌표
                    x--;
                    y--;
                }
            	tr[y][x] = num++;                
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
            	if(tr[i][j] == 0) break;
            	answer[tmp] = tr[i][j];
            	tmp++;
            }
        }
          
        System.out.println(Arrays.deepToString(tr));
//        System.out.println(Arrays.toString(answer));
        
        return answer;
    }
}
