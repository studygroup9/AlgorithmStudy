package test;

import javax.xml.soap.Node;

public class W20_Q1 {
	/**
	 * head정렬된 연결 목록이 주어지면 각 요소가 한 번만 나타나도록 모든 중복 항목을 삭제합니다 . 돌아 가기 링크 된 목록을 분류 뿐만 아니라 .
	 * 
	 * Input: head = [1,1,2,3,3]
	 * Output: [1,2,3]
	 * 
	 * Definition for singly-linked list.

	 */
	
	public static ListNode deleteDuplicates(ListNode head) {
        ListNode a = head;
        
        while(a != null && a.next != null){
            if(a.val == a.next.val){
                a.next = a.next.next;
            }else{
                a = a.next;
            }
        }
        
        return head;
        
    }

}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
