package test;

public class W20_Q2 {

	//Input: head = [1,1,2]
	//Output: [1,2]
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(climbStairs(10));
	}

	/*
     * n = 3 -> 3;/
     * 1+1+1
     * 2+1
     * 1+2
     * 
     * i = 4 -> 5;
     * 1+1+1+1
     * 1+1+2
     * 1+2+1
     * 2+1+1
     * 2+2
     * 
     * N = 5 -> 8;
     * 1+1+1+1+1
     * 1+1+1+2
     * 1+1+2+1
     * 1+2+1+1
     * 2+1+1+1
     * 1+2+2
     * 2+1+2
     * 2+2+1
     * 
     *  n = 6 -> 13;
     *  1+1+1+1+1+1
     *  1+1+1+1+2
     *  1+1+1+2+1
     *  1+1+2+1+1
     *  1+2+1+1+1
     *  2+1+1+1+1
     *  1+1+2+2
     *  1+2+1+2
     *  2+1+1+2
     *  1+2+2+1
     *  2+1+2+1
     *  2+2+1+1
     *  2+2+2
     */
	public static int climbStairs(int n) {
        int[] a = new int[n + 2];
        a[1] = 1; 
        a[2] = 2;
         
         for(int i = 3; i <=n ; i++)
         {
             a[i] = a[i - 1] + a[i - 2];
             System.out.println(i +"->" +a[i - 1] +": "+(i - 1) + ", "+ a[i - 2] +": " +( i - 2 )+", " +a[i]);
             
         }
         
         
        return a[n];  
    }
}
