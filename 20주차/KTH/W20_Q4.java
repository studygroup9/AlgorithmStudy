package test;

import java.util.Arrays;

public class W20_Q4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solution(new int[] {1,3,2,5,4},	9));
	}
	public static int solution(int[] d, int budget) {
		int answer = 0;
        
        Arrays.sort(d);
        int tmp = 0;
        
        for(int i = 0; i < d.length; i++) {

        	if(d[i] > budget) 
        		break;
        	
        	budget -= d[i];
        	answer++;
        }
        
        return answer;
    }
}
