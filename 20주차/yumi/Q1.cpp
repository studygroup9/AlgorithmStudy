// ���� : Climbing Stairs
//Input: n = 2 Output: 2
//Input: n = 3 Output: 3

class Solution {
public:
    int climbStairs(int n) {
        
       if  ((n == 1) or (n == 2)){
            return n;
       }
           
        int n1 =1 , n2 =2 , cur_n =3;
        int ways;
            
        while (cur_n <= n){
            ways = n1 + n2 ;
            n1 = n2;
            n2 = ways; 
            cur_n++;  
        }
        
        return ways ;
        
    }
};