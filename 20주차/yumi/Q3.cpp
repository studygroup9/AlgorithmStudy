// 부족한 금액 계산하기
// 이용금액이 3인 놀이기구를 4번 타고 싶은 고객이 현재 가진 금액이 20이라면, 총 필요한 놀이기구의 이용 금액은 30 (= 3+6+9+12) 이 되어 10만큼 부족하므로 10을 return 합니다.


#include <iostream>

using namespace std;

long long solution(int price, int money, int count)
{
    long long answer = money;
    int t_price;
    
    for(int i = 1 ; i <= count; i++ ){
    
        t_price = price * i;
        answer = answer - t_price;
        
    }

    if (answer > 0)
        return 0;
    else 
        return abs(answer);
         
    return answer;
}