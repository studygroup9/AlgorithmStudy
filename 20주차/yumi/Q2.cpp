//���� : Remove Duplicates from Sorted List
//Input: head = [1,1,2] Output: [1,2] 
//Input: head = [1,1,2,3,3] Output: [1,2,3]

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        ListNode* ls = head;
    
        while (ls == NULL or ls->next == NULL ){
            if (ls->val == ls->next->val){
                    ls->next = ls->next->next;
                }else{
                    ls = ls->next;}
        } 
        
        return head;
    }
};