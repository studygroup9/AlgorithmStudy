# **20주차 문제**

- 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.

1. [83. Remove Duplicates from Sorted List: leetCode Easy](https://leetcode.com/problems/remove-duplicates-from-sorted-list/)

2. [70. Climbing Stairs: leetCode Easy](https://leetcode.com/problems/climbing-stairs/)

3. [부족한 금액 계산하기 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/82612)

4. [예산 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/12982)

5. [삼각 달팽이 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/68645)

6. [가장 먼 노드 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/49189)

화이팅 :fire: 👍:construction_worker: 👍 :fire:
