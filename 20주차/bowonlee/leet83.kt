package week20

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */
class leet83 {
    fun deleteDuplicates(head: ListNode?): ListNode? {
        if(head?.next == null) return head

        var node = head

        while (node?.next != null){
            if(node.`val` == node.next!!.`val`){
                node.next = node.next!!.next
            } else{
                node = node.next
            }

        }

        return head
    }
}

class ListNode(var `val`: Int) {
    var next: ListNode? = null
}