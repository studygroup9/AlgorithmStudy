package week20

/** 예산 */
class PR82612 {
    fun solution(price: Int, money: Int, count: Int): Long {
        var answer: Long = -1

        for(i in 1 .. count){
            answer += price * i.toLong()
        }

        if(answer <= money){
            return 0
        }

        return answer - money
    }
}