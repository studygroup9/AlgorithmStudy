package week20

class leet70 {
    fun climbStairs(n: Int): Int {
        val answer = IntArray(n + 1)

        if(n<3){
            return n
        }

        answer[1] = 1
        answer[2] = 2



        for(i in 3..n){
            answer[i] = answer[i-1] + answer[i-2]
        }

        return answer[n]

    }

}

fun main(){
    val sol = leet70()
    println(sol.climbStairs(3))
    println(sol.climbStairs(2))
    println(sol.climbStairs(44))
}