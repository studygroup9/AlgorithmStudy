package week5


/**
 * 주어진 숫자 중 3개의 수를 더했을 때 소수가 되는 경우의 개수를 구하려고 합니다.
 * 숫자들이 들어있는 배열 nums가 매개변수로 주어질 때,
 * nums에 있는 숫자들 중 서로 다른 3개를 골라 더했을 때 소수가 되는 경우의 개수를 return 하도록 solution 함수를 완성해주세요.
 *
 * */

class PR12977PrimeNumber {
    fun solution(nums: IntArray): Int {
        var answer = 0


        for(i in 0 until nums.size-2){
            for(j in i + 1 until  nums.size - 1){
                for(k in j + 1 until  nums.size){
                    val num = nums[i] + nums[j] +nums[k]
                    if(checkPrime(num)){
                        answer++
                    }
                }
            }
        }


        return answer
    }

    private fun checkPrime(num:Int): Boolean{
        for( i in 2 .. num/2){
            if (num % i == 0) {
                return false
            }
        }
        return true
    }
}




fun main(){
    val sol = PR12977PrimeNumber()

    println(sol.solution(intArrayOf(1,2,3,4)))

}