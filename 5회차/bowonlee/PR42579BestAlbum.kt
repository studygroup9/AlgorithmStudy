package week5

import java.util.*
class Solution {
    fun solution(genres: Array<String>, plays: IntArray): IntArray {
        var answer = intArrayOf()

        var answerList = mutableListOf<Int>()

        // 내부 맵의 정의 <횟수, 고유번호>
        var genresAndPlayHm = hashMapOf<String,HashMap<Int,Int>>()

        var genresTotalSortMap = sortedMapOf<Int,String>()

        //각 장르별 리스트
        plays.forEachIndexed{index, it->
            if(genresAndPlayHm.contains(genres[index])){
                // 기존 리스트에 추가
                genresAndPlayHm.get(genres[index])?.put(index,it)


            }else{
                //새로운 리스트 생성
                genresAndPlayHm.put(genres[index], hashMapOf())
                genresAndPlayHm.get(genres[index])?.put(index,it)

            }

        }


        genresAndPlayHm.forEach { genre : String, pair ->

            genresTotalSortMap.put(pair.values.sum() ,genre)

        }



        // 고유번호 - 재생횟수
        lateinit var tempList : MutableList<Pair<Int,Int>>
        lateinit var tempAnswerList : MutableList<Int>

        genresTotalSortMap.values.reversed().forEach { genre ->
            tempList = mutableListOf()
            tempAnswerList = mutableListOf()
            genresAndPlayHm.get(genre)?.forEach {
                tempList.add(Pair(it.key,it.value))
            }

            tempList.sortByDescending {
                it.second
            }

            tempList.forEachIndexed { index, pair ->
                if(tempAnswerList.isEmpty()){
                    tempAnswerList.add(pair.first)
                }else{
                    if(tempList[index-1].second == pair.second){
                        if(tempList[index-1].first<pair.first){
                            tempAnswerList.add(pair.first)
                        }else{
                            val tempVal = tempAnswerList[tempAnswerList.lastIndex]
                            tempAnswerList[tempAnswerList.lastIndex] = pair.first
                            tempAnswerList.add(tempVal)
                        }
                    }else{
                        tempAnswerList.add(pair.first)
                    }
                }

            }

            tempAnswerList.forEachIndexed { index, i ->
                if(index>=2){return@forEachIndexed}
                answerList.add(i)
            }

        }



        answer = answerList.toIntArray()


        return answer
    }
}


class Solution_2 {
    fun solution(genres: Array<String>, plays: IntArray): IntArray {
        return genres.indices.groupBy {
            // 그룹 별로 인덱스로 모음  < 장르,
            genres[it] }
            .toList()
            .sortedByDescending {
                // 많은개 위로 올라와야 하므로 역순 정렬
                it.second.sumBy { plays[it] } // 같은 이름으로 묶인 것들의 총합
            }
            .map {
                // 장르 내부에서 2차 정렬
                it.second.sortedByDescending {
                    plays[it]
                }.take(2)
            }.flatten()
            .toIntArray()
    }
}

