# **5주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [소수 만들기: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12977)

2. [내적: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/70128)

3. [다리를 지나는 트럭: Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42583)

4. [베스트앨범: Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42579)

화이팅  :fire: 👍:construction_worker: 👍 :fire:   
