package test;

import java.util.Arrays;

public class W5_Q2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] a = {1,2,3,4};
		int [] b = {-3,-1,0,2};
		System.out.println(solution(a, b));
		
	}
	
	/*
	 * 내적 구하기
		 길이가 같은 두 1차원 정수 배열 a, b가 매개변수로 주어집니다. a와 b의 내적을 return 하도록 solution 함수를 완성해주세요.
		이때, a와 b의 내적은 a[0]*b[0] + a[1]*b[1] + ... + a[n-1]*b[n-1] 입니다. (n은 a, b의 길이)
		
		테스트 1 〉	통과 (0.04ms, 53.5MB)
		테스트 2 〉	통과 (0.03ms, 53.3MB)
		테스트 3 〉	통과 (0.02ms, 53.1MB)
		테스트 4 〉	통과 (0.02ms, 53.4MB)
		테스트 5 〉	통과 (0.03ms, 53MB)
		테스트 6 〉	통과 (0.05ms, 54MB)
		테스트 7 〉	통과 (0.04ms, 52.1MB)
		테스트 8 〉	통과 (0.04ms, 52MB)
		테스트 9 〉	통과 (0.05ms, 52.2MB)
	 */
	
	
	static int solution(int[] a, int[] b) {
	    int answer = 0;
	    
	    for(int i = 0; i < a.length; i++) {
	    	answer = answer + (a[i] * b[i]); 
	    }
	    
	    return answer;
	}
	
	
}
