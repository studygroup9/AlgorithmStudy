package test;

import java.util.Arrays;

public class W5_Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] nums = {1,2,3,4};
		solution(nums);
		
	}
	
	/*
	 * 소수만들기
	 	: 주어진 숫자 중 3개의 수를 더했을 때 소수가 되는 경우의 개수를 구하려고 합니다. 
	 	     숫자들이 들어있는 배열 nums가 매개변수로 주어질 때, nums에 있는 숫자들 중 서로 다른 3개를 골라 더했을 때 소수가 되는 경우의 개수를 return 하도록 solution 함수를 완성해주세요.
	  	
	  	테스트 1 〉	통과 (45.64ms, 53.5MB)
		테스트 2 〉	통과 (50.49ms, 54.2MB)
		테스트 3 〉	통과 (24.12ms, 53.6MB)
		테스트 4 〉	통과 (22.97ms, 53.7MB)
		테스트 5 〉	통과 (52.77ms, 55MB)
		테스트 6 〉	통과 (55.41ms, 53.5MB)
		테스트 7 〉	통과 (21.50ms, 53.3MB)
		테스트 8 〉	통과 (79.37ms, 54.6MB)
		테스트 9 〉	통과 (23.36ms, 53.3MB)
		테스트 10 〉	통과 (69.18ms, 53.9MB)
		테스트 11 〉	통과 (13.79ms, 53.9MB)
		테스트 12 〉	통과 (13.72ms, 52.9MB)
		테스트 13 〉	통과 (16.02ms, 53.5MB)
		테스트 14 〉	통과 (13.52ms, 53.7MB)
		테스트 15 〉	통과 (21.64ms, 52.3MB)
		테스트 16 〉	통과 (67.44ms, 53.4MB)
		테스트 17 〉	통과 (1.74ms, 52.6MB)
		테스트 18 〉	통과 (14.58ms, 53.3MB)
		테스트 19 〉	통과 (21.19ms, 53.7MB)
		테스트 20 〉	통과 (90.81ms, 54MB)
		테스트 21 〉	통과 (102.33ms, 57.9MB)
		테스트 22 〉	통과 (0.38ms, 52.8MB)
		테스트 23 〉	통과 (0.03ms, 52MB)
		테스트 24 〉	통과 (81.12ms, 54.7MB)
		테스트 25 〉	통과 (84.51ms, 53.5MB)
		테스트 26 〉	통과 (15.98ms, 53.6MB)
	 * */
	
	static int solution(int [] nums) {
	    int answer = 0;
	    int a[] = new int[3];
	    
	    for(int i = 0; i < nums.length - 2; i++) {
	    	for(int j = i + 1; j < nums.length -1; j ++) {
	    		for(int k = j + 1; k < nums.length; k++) {
	    			a[0] = nums[i];
	    			a[1] = nums[j];
	    			a[2] = nums[k];
	    			boolean flg = true;	    			
	    			int result = a[0] + a[1] + a[2];
	    			
	    			for(int z = 2; z*z <= result; z++) {
	    				if(result % z == 0) {
	    					flg = false;
	    					break;
	    				}
	    			}
	    			
	    			if(flg == true) {
	    				System.out.println(Arrays.toString(a) + "를 이용해서 " + result + "을 만들 수 있습니다.");
	    				answer++;
	    			}
	    		}
	    	}
	    }
	    
	    return answer;
	}
	
}
