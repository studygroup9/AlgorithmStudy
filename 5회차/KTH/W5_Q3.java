package test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class W5_Q3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int bridge_lenth = 2;
		int weight = 10;
		int [] truck_weights = {7,4,5,6};
		System.out.println(solution(bridge_lenth, weight, truck_weights));
		System.out.println("===========================================");
		System.out.println("===========================================");
		bridge_lenth = 100;
		weight = 100;
		int [] truck_weights2 = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
//		System.out.println(solution(bridge_lenth, weight, truck_weights2));
		
	}
	
	/*
	 * 다리를 지나는 트럭
		트럭 여러 대가 강을 가로지르는 일 차선 다리를 정해진 순으로 건너려 합니다. 모든 트럭이 다리를 건너려면 최소 몇 초가 걸리는지 알아내야 합니다. 트럭은 1초에 1만큼 움직이며, 다리 길이는 bridge_length이고 다리는 무게 weight까지 견딥니다.
 		트럭이 다리에 완전히 오르지 않은 경우, 이 트럭의 무게는 고려하지 않습니다.
		예를 들어, 길이가 2이고 10kg 무게를 견디는 다리가 있습니다. 무게가 [7, 4, 5, 6]kg인 트럭이 순서대로 최단 시간 안에 다리를 건너려면 다음과 같이 건너야 합니다.
		
		테스트 1 〉	통과 (2.09ms, 52.2MB)
		테스트 2 〉	통과 (12.68ms, 53.6MB)
		테스트 3 〉	통과 (0.16ms, 52.8MB)
		테스트 4 〉	통과 (12.53ms, 54.3MB)
		테스트 5 〉	통과 (47.99ms, 60.6MB)
		테스트 6 〉	통과 (20.78ms, 54.4MB)
		테스트 7 〉	통과 (2.96ms, 52.4MB)
		테스트 8 〉	통과 (1.88ms, 52.9MB)
		테스트 9 〉	통과 (5.33ms, 53.2MB)
		테스트 10 〉	통과 (0.84ms, 51.8MB)
		테스트 11 〉	통과 (0.14ms, 52.6MB)
		테스트 12 〉	통과 (1.03ms, 52.9MB)
		테스트 13 〉	통과 (1.98ms, 52.3MB)
		테스트 14 〉	통과 (0.22ms, 52.7MB)
	 */
	
	
	static int solution(int bridge_length, int weight, int[] truck_weights) {
        int answer = 0;
        Queue<Integer> bl = new LinkedList<Integer>();
        int endPoint = 0;
        int a = 0;
        int curWeight = 0;
        
        while (true) {
        	
        	if (bridge_length < answer) {
        		if (bl.peek() != 0) {
        			curWeight = curWeight - bl.peek();
            		bl.remove();
            		endPoint++;
            		if(endPoint == truck_weights.length) break;
            		
            	}else{
            		bl.remove();
            	}
        	}
        	
        	if(answer > 0 && truck_weights.length > a && weight >= (curWeight + truck_weights[a])) {
        		curWeight += truck_weights[a];
        		bl.offer(truck_weights[a]);
        		a++;
        	}else if(answer > 0){
        		bl.offer(0);
        	}

        	answer++;
        }
        
        return answer;
    }
	
	
}
