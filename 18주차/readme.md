# **18주차 문제**

- 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.

1. [Longest Common Prefix: leetCode Easy](https://leetcode.com/problems/longest-common-prefix/)

2. [Merge Two Sorted Lists: leetCode Easy](https://leetcode.com/problems/merge-two-sorted-lists/)

3. [메뉴 리뉴얼 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/72411)

4. [가장 먼 노드 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/49189)

- ex) [N으로 표현 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42895)

Lv3 N으로 표현은 추가 문제로 넘기고 새로은 Lv3 추가 했습니다~

화이팅 :fire: 👍:construction_worker: 👍 :fire:
