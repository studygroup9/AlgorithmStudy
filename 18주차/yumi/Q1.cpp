//Merge Two Sorted Lists
//Input: l1 = [1,2,4], l2 = [1,3,4]
//Output: [1,1,2,3,4,4]
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        
        ListNode l3(0);
        
        auto curr = &l3;

        while (l1 && l2) {
            
            if (l1->val <= l2->val) {
                curr->next = l1;
                l1 = l1->next;
            } else {
                curr->next = l2;
                l2 = l2->next;
            }
            curr = curr->next;
        }
        //����
        if(l1 == NULL) curr->next = l2;
            else curr->next = l1;
        
        return l3.next;
    }
        
};