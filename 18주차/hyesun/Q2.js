// 메뉴 리뉴얼
// https://programmers.co.kr/learn/courses/30/lessons/72411

function solution(orders, course) {
  let menu = new Map();

  course.map((pick) => {
    orders.map((order) => {
      menu = combination(order, pick, menu);
    });
  });

  let answer = [...menu]
    .filter((v) => v[1] > 1)
    .sort((a, b) => {
      if (a[0].length < b[0].length) {
        return 1;
      }

      if (a[0].length === b[0].length) {
        if (a[1] < b[1]) {
          return 1;
        } else {
          return -1;
        }
      }
    });

  let result = [];
  course.map((pick) => {
    let g = answer.filter((item) => item[0].length === pick);
    let h = new Map(g);
    let max = Math.max(...h.values());

    for (const [key, value] of h) {
      if (value < max) {
        break;
      }
      result.push(key);
    }
  });

  return result.sort();
}

function combination(letters, pickTargetCount, menu) {
  let recur = (lastIdx, str = '') => {
    if (str.length === pickTargetCount) {
      let str2 = str.split('').sort().join('');
      if (menu.has(str2)) {
        menu.set(str2, menu.get(str2) + 1);
      } else {
        menu.set(str2, 1);
      }
      return menu;
    }

    for (let i = lastIdx + 1; i < letters.length; i++) {
      recur(i, str + letters[i]);
    }
  };

  recur(-1);
  return menu;
}

/**
테스트 1 〉	통과 (0.70ms, 30.1MB)
테스트 2 〉	통과 (0.57ms, 30MB)
테스트 3 〉	통과 (0.78ms, 30.1MB)
테스트 4 〉	통과 (0.49ms, 30.2MB)
테스트 5 〉	통과 (0.76ms, 30.1MB)
테스트 6 〉	통과 (1.19ms, 30.1MB)
테스트 7 〉	통과 (1.31ms, 30.3MB)
테스트 8 〉	통과 (10.45ms, 34.8MB)
테스트 9 〉	통과 (9.85ms, 34.8MB)
테스트 10 〉	통과 (8.50ms, 34.9MB)
테스트 11 〉	통과 (8.99ms, 34.1MB)
테스트 12 〉	통과 (6.38ms, 34.2MB)
테스트 13 〉	통과 (8.86ms, 34.8MB)
테스트 14 〉	통과 (7.72ms, 34.5MB)
테스트 15 〉	통과 (8.41ms, 34.7MB)
테스트 16 〉	통과 (4.27ms, 33.5MB)
테스트 17 〉	통과 (21.76ms, 33.3MB)
테스트 18 〉	통과 (5.13ms, 32.8MB)
테스트 19 〉	통과 (4.20ms, 32.6MB)
테스트 20 〉	통과 (6.54ms, 33.4MB)
 */
