// Longest Common Prefix
// https://leetcode.com/problems/longest-common-prefix/

/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function (strs) {
  strs = strs.sort((a, b) => a.length - b.length);

  let answer = '';
  const firstValueToArr = strs[0].split('');

  for (let i = 0; i < firstValueToArr.length; i++) {
    const isPass = strs.every((v) => v.charAt(i) === firstValueToArr[i]);
    if (isPass) {
      answer += firstValueToArr[i];
      continue;
    }
    return answer;
  }

  return answer;
};
