package week18

class LeetCode21 {
    fun mergeTwoLists(l1: ListNode?, l2: ListNode?): ListNode? {

        if(l1 == null) return l2
        if(l2 == null) return l1


        return if(l1.`val` < l2.`val` ) {
            l1.next = mergeTwoLists(l1.next,l2)
            l1
        } else{
            l2.next = mergeTwoLists(l1, l2.next)
            l2
        }

    }


}

// 순수한 링크드 리스트. 정렬 알고리즘 직접 구현하라는 뜻으로 보입니다.
// 노드를 중심으로 생각
class ListNode(var `val`: Int) {
         var next: ListNode? = null
}


fun main(){

    var sol = LeetCode21()

    sol.mergeTwoLists( null,null )

}