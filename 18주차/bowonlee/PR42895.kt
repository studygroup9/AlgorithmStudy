package week18

import kotlin.math.min

class PR42895 {

    var result = 9
    var target = 0

    fun solution(N: Int, number: Int): Int {
        var answer = 0

        target = number
        logic(N,0,0)

        if (result == 9) {
            return -1
        }
        return result

    }

/**
만들 수 있는 모든 경우의 수 탐색
 */
    fun logic(n: Int, cnt: Int, current: Int){
        if(cnt > 8){
            return
        }

        if(current == target){
            result = min(result,cnt)
            return
        }

        var nextNum = n
        for (i in 0 until 8 - cnt) {
            logic(n, cnt+1 + i, current + nextNum)
            logic(n, cnt+1 + i, current - nextNum)
            logic(n, cnt+1 + i, current * nextNum)
            logic(n, cnt+1 + i, current / nextNum)
            nextNum += n * Math.pow(10.0, i + 1.toDouble()).toInt()
        }

    }
}

fun main(){
        var sol = PR42895()
    println(sol.solution(2,11))
    println(sol.solution(5,12))
}

/**
테스트 1 〉	통과 (10.49ms, 54.6MB)
테스트 2 〉	통과 (8.19ms, 55.3MB)
테스트 3 〉	통과 (11.49ms, 55.2MB)
테스트 4 〉	통과 (8.44ms, 54.4MB)
테스트 5 〉	통과 (7.96ms, 54.9MB)
테스트 6 〉	통과 (11.75ms, 54.3MB)
테스트 7 〉	통과 (7.84ms, 55.8MB)
테스트 8 〉	통과 (8.19ms, 54.6MB)
테스트 9 〉	통과 (5.30ms, 54.6MB)
 */