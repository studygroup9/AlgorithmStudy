// 행렬의 덧셈
// https://programmers.co.kr/learn/courses/30/lessons/12950

function solution(arr1, arr2) {
  return arr1.reduce((acc, value, index) => {
    let inner = value.map((v, i) => v + arr2[index][i]);
    acc.push(inner);
    return acc;
  }, []);
}

/**
테스트 1 〉	통과 (0.06ms, 29.8MB)
테스트 2 〉	통과 (0.18ms, 30.2MB)
테스트 3 〉	통과 (0.30ms, 30.5MB)
테스트 4 〉	통과 (0.20ms, 30.1MB)
테스트 5 〉	통과 (0.09ms, 30.1MB)
테스트 6 〉	통과 (0.20ms, 30.2MB)
테스트 7 〉	통과 (0.07ms, 29.6MB)
테스트 8 〉	통과 (0.17ms, 29.9MB)
테스트 9 〉	통과 (0.50ms, 33.9MB)
테스트 10 〉	통과 (0.71ms, 33.7MB)
테스트 11 〉	통과 (0.26ms, 33.5MB)
테스트 12 〉	통과 (0.54ms, 34MB)
테스트 13 〉	통과 (0.46ms, 33.8MB)
테스트 14 〉	통과 (0.52ms, 33.7MB)
테스트 15 〉	통과 (0.58ms, 33.8MB)
테스트 16 〉	통과 (0.33ms, 33.9MB)
테스트 17 〉	통과 (7.24ms, 62.5MB)
 */
