import kotlin.math.max
import kotlin.math.min
class Solution {
    fun solution(arr: IntArray): Int {
        return arr.reduce { acc, i -> acc*i/gcd(acc,i)}
    }

    // 유클리드 호제법
    fun gcd(a: Int, b: Int): Int {
        val maximum = max(a, b)
        val minimum = min(a, b)

        return if (minimum == 0) {
             max(a, b)
        } else {
             gcd(minimum, maximum % minimum)
        }
    }

}

/**
테스트 1 〉	통과 (16.06ms, 57.6MB)
테스트 2 〉	통과 (16.54ms, 57.8MB)
테스트 3 〉	통과 (16.11ms, 57.8MB)
테스트 4 〉	통과 (19.27ms, 58MB)
테스트 5 〉	통과 (16.57ms, 57.4MB)
테스트 6 〉	통과 (15.94ms, 57.6MB)
테스트 7 〉	통과 (16.20ms, 57.7MB)
테스트 8 〉	통과 (16.00ms, 57.9MB)
테스트 9 〉	통과 (12.47ms, 57.2MB)
테스트 10 〉	통과 (16.34ms, 57.7MB)
 */