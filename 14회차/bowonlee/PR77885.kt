
import kotlin.math.pow
class Solution {
    fun solution(numbers: LongArray): LongArray {

        var answer: LongArray = longArrayOf()

        return numbers.map {
            if(it % 2.0 == 0.0){
                it + 1
            } else {
                it + this.logic(it)

            }
        }.toLongArray()


    }

    /**
     * 짝수인 경우 (가장 뒤의 비트가 0)
     * 가장 뒤의 비트다 0이므로 무조건 + 1
     * 홀수인 경우 (가장 뒤의 비트가 1)
     * 가장 뒤의 0 앞의 1의 위치의 자릿 수 만큼 더하기
     *   1110111 -> 1111011 
     *   1110111
     * + 0000100
     *   1111011
     *
     * 뭔가 순수 비트 연산으로도 가능은 할 법하다.. 비트 뒤집을 시 부호가 달라지는 문제 발생..
     */
    fun logic(num: Long): Long{
        num.toString(2).indexOfLast { it == '0' }
        val temp =  num.toString(2).reversed().indexOfFirst { it == '0' }

        return if(temp  == -1){
            2.0.pow(num.toString(2).length -1).toLong()
        } else {
            2.0.pow(temp -1).toLong()
        }
    }
}

/**

테스트 1 〉	통과 (22.14ms, 57.3MB)
테스트 2 〉	통과 (124.61ms, 86.9MB)
테스트 3 〉	통과 (22.45ms, 56.6MB)
테스트 4 〉	통과 (18.58ms, 57.5MB)
테스트 5 〉	통과 (28.96ms, 57.1MB)
테스트 6 〉	통과 (20.87ms, 56.8MB)
테스트 7 〉	통과 (124.04ms, 95.1MB)
테스트 8 〉	통과 (159.04ms, 98.8MB)
테스트 9 〉	통과 (173.06ms, 98.3MB)
테스트 10 〉	통과 (197.07ms, 109MB)
테스트 11 〉	통과 (216.96ms, 113MB)

 */