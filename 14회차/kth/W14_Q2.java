package test;

import java.util.Arrays;

public class W14_Q2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [][] a = {{1,2},{2,3}};
		int [][] b = {{3,4},{5,6}};
		System.out.println(solution(a,b));
		
	}

	public static int[][] solution(int[][] arr1, int[][] arr2) {
        int[][] answer = new int [arr1.length][arr1[0].length];
        
        for(int i = 0; i < arr1.length; i++) {
        	for(int j = 0; j < arr1[i].length; j++) {
            	// System.out.println(arr1[i][j] + ","+ arr2[i][j]);
            	answer[i][j] = arr1[i][j] + arr2[i][j];
            }	
        }
        
//        System.out.println(Arrays.deepToString(answer));
        
        return answer;
    }

}
