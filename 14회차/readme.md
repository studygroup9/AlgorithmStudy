# **14주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [핸드폰 번호 가리기 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12948)

2. [행렬의 덧셈 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12950)

3. [N개의 최소공배수 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/12953)

4. [N으로 표현 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42895)

화이팅  :fire: 👍:construction_worker: 👍 :fire: