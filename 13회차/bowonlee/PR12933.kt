class Solution {
    fun solution(n: Long): Long =
         n.toString().map { it.toString().toInt() }.sortedDescending().joinToString ("") .toLong()
    
}