
import kotlin.math.sqrt
class Solution {
    fun solution(n: Long): Long {
            var temp = sqrt(n.toFloat())

        if(temp%1.0 == 0.0){
            return (temp.toLong() + 1)*(temp.toLong() + 1)
        }

        return -1
    }
}