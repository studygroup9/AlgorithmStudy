// 정수 : 정수내림차순으로 정렬하기 
// n의 각 자릿수를 큰것부터 작은 순으로 정렬한 새로운 정수를 리턴해주세요. 예를들어 n이 118372면 873211을 리턴하면 됩니다.
#include <string>
#include<algorithm>

using namespace std;

long long solution(long long n) {
    long long answer = 0;
    string nn = to_string(n);
    sort(nn.begin(), nn.end(),greater<char>());
    return answer = stoll(nn);
}