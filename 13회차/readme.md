# **13주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [정수 내림차순으로 배치하기 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12933)

2. [정수 제곱근 판별 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12934)

3. [2개 이하로 다른 비트 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/77885)

4. [N으로 표현 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42895)

화이팅  :fire: 👍:construction_worker: 👍 :fire: