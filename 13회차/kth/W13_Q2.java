package test;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

public class W13_Q2 {

	
	/*
	 * 정수 내림차순으로 배치하기
		문제 설명
		함수 solution은 정수 n을 매개변수로 입력받습니다. n의 각 자릿수를 큰것부터 작은 순으로 정렬한 새로운 정수를 리턴해주세요. 예를들어 n이 118372면 873211을 리턴하면 됩니다.
		
		제한 조건
		n은 1이상 8000000000 이하인 자연수입니다.
		
		n	return
		118372	873211
	 * */
   public static void main(String[] args) {
      // TODO Auto-generated method stub

      
      System.out.println(solution(118372));
      
   }
   public static long solution(long n) {
       long answer = 0;
       
       int [] l = Stream.of(String.valueOf(n).split("")).mapToInt(Integer::parseInt).toArray();
       String s = Arrays.toString(Arrays.stream(l).boxed().sorted(Collections.reverseOrder()).mapToInt(Integer::new).toArray()).replaceAll("[^0-9]", "");
       
       answer = Integer.parseInt(s);
       
       return answer;
   }


}