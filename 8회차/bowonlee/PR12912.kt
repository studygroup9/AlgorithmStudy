package week8

import kotlin.math.abs

class PR12912 {
    fun solution(a: Int, b: Int): Long {
        var answer: Long = 0
        val length = abs(a-b).toLong() + 1
        val sum = (a+b).toLong()
        if( (length) % 2 == 0L){
            answer = sum * length/2
        } else{
            answer = sum * (length-1)/2 + sum/2
        }


        return answer
    }
}
fun main(){
    val sol = PR12912()
    println(sol.solution(3,6))
    println(sol.solution(3,5))
    println(sol.solution(3,3))
    println(1/2)
}
/**
 *
 * 테스트 1 〉	통과 (0.04ms, 54.4MB)
테스트 2 〉	통과 (0.04ms, 54.9MB)
테스트 3 〉	통과 (0.05ms, 54.8MB)
테스트 4 〉	통과 (0.05ms, 54.8MB)
테스트 5 〉	통과 (0.05ms, 54.3MB)
테스트 6 〉	통과 (0.04ms, 54.8MB)
테스트 7 〉	통과 (0.04ms, 54.7MB)
테스트 8 〉	통과 (0.04ms, 55.3MB)
테스트 9 〉	통과 (0.09ms, 54.5MB)
테스트 10 〉	통과 (0.05ms, 54.7MB)
테스트 11 〉	통과 (0.04ms, 54.6MB)
테스트 12 〉	통과 (0.04ms, 54.7MB)
테스트 13 〉	통과 (0.06ms, 54.9MB)
테스트 14 〉	통과 (0.04ms, 55.7MB)
테스트 15 〉	통과 (0.05ms, 54.9MB)
테스트 16 〉	통과 (0.05ms, 54.9MB)
 * */