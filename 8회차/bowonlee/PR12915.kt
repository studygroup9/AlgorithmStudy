package week8

/**
 * 문자열 내 마음대로 정렬하기
 * */
class PR12915 {
    fun solution(strings: Array<String>, n: Int): Array<String> =
         strings.sortedWith(compareBy({ it[n] }, {it})  ).toTypedArray()

}