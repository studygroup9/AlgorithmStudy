// 문제  : 두 정수 사이의 합
// 두 정수 a, b가 주어졌을 때 a와 b 사이에 속한 모든 정수의 합을 리턴하는 함수,
// solution을 완성하세요.
// 예를 들어 a = 3, b = 5인 경우, 3 + 4 + 5 = 12이므로 12를 리턴합니다.

#include <string>
#include <vector>

using namespace std;

long long solution(int a, int b) {
    long long answer = 0;
    int x, y;
    long long sum = 0;

    if (a != b) {
        if (a > b) {
            x = a;
            y = b;
        }else{
            x = b;
            y = a;
        }
        while (y <= x) {
            sum = sum + y;
            y++;
        }
        answer = sum;    
        
    } else {
        answer = a;    
    }
    
    return answer;
}

/// 다른사람풀이 
/// 시그마 합 공식 1~n 까지 자연수의 합 : n(n+1)/2 
#include <string>
#include <vector>

using namespace std;

long long solution(int a, int b) {
    return (long long)(a + b) * (abs(a - b) + 1) / 2;
}