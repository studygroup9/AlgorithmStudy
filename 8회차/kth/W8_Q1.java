package test;

import java.util.ArrayList;
import java.util.Collections;

public class W8_Q1 {

	/*
	 * 두 정수 사이의 합
	 * 
	 * 두 정수 a, b가 주어졌을 때 a와 b 사이에 속한 모든 정수의 합을 리턴하는 함수, solution을 완성하세요.
	 * 예를 들어 a = 3, b = 5인 경우, 3 + 4 + 5 = 12이므로 12를 리턴합니다.
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(solution(10, 1));
		
	}
	
	 public static long solution(int a, int b) {
	        return sumAtoB(Math.min(a, b), Math.max(b, a));
    }

    private static long sumAtoB(long a, long b) {
        return (b - a + 1) * (a + b) / 2;
    }

}

