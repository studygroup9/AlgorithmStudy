package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Collections;

public class W8_Q2 {

	/*
	 * 문자열 내 마음대로 정렬하기
	 * 문자열로 구성된 리스트 strings와, 정수 n이 주어졌을 때, 각 문자열의 인덱스 n번째 글자를 기준으로 오름차순 정렬하려 합니다. 
	 * 예를 들어 strings가 ["sun", "bed", "car"]이고 n이 1이면 각 단어의 인덱스 1의 문자 "u", "e", "a"로 strings를 정렬합니다.
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String [] a = {"abce", "abcd", "cdx"};
		int b = 2;
		System.out.println(solution(a, b));
		
	}
	
	public static ArrayList<String> solution(String[] strings, int n) {
	    ArrayList<String> answer = new ArrayList<String> (); 
	    
	    Arrays.sort(strings);
	    
	    for (int i = 0; i < strings.length; i++) {
	    	answer.add(strings[i]);
	    }
	    
	    Comparator<String> comparator = (o1, o2) -> {
	    	int s1 = o1.substring(n, n+1).hashCode();
	    	int s2 = o2.substring(n, n+1).hashCode();
	    	
	        return s1 > s2 ? 1 : s1 == s2 ? 0 : -1 ;
	    };
	    
	    
	    answer.sort(comparator);
	    
		
	    return answer;
	}

}

