package test;
//
public class W19_Q3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solution("one4seveneight"));
	}
	public static int solution(String s) {
        String[] str = new String [] {"zero","one","two","three","four","five"
									 ,"six","seven","eight","nine"};
		
		for(int i = 0; i <= 9; i++) {
			s = s.replaceAll(str[i], Integer.toString(i));
		}
        
		return Integer.parseInt(s);
    }
}
