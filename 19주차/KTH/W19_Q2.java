package test;
//
import java.util.Arrays;

public class W19_Q2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(searchInsert(new int[] {1,3,5,6}, 5));
	}
	public static int searchInsert(int[] nums, int target) {
        int a = Arrays.binarySearch(nums, target);
    	
    	if(a < 0){
    		a = 0;
    		for(int i = 0; i < nums.length; i++) {
    			if(nums[i] > target) {
    				break;
    			}else {
    				a++;
    			}
    		}
        }
        
        
         return a;
    }
}
