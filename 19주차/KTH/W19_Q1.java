package test;
//
public class W19_Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(maxSubArray(new int[] {-2,1,-3,4,-1,2,1,-5,4}));
	}
	public static int maxSubArray(int[] nums) {
        int sum = 0;
        int max = Integer.MIN_VALUE;
       
        for(int num : nums) {
            sum = Math.max(sum,0) + num;
        	max = Math.max(max, sum);
        }
       
       	return max;
    }

}
