# **19주차 문제**

- 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.

1. [Maximum Subarray: leetCode Easy](https://leetcode.com/problems/maximum-subarray/)

2. [Search Insert Position: leetCode Easy](https://leetcode.com/problems/search-insert-position/)

3. [숫자 문자열과 영단어 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/81301)

4. [폰켓몬 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/1845)

5. [최댓값과 최솟값 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/12939)

6. [가장 먼 노드 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/49189)

화이팅 :fire: 👍:construction_worker: 👍 :fire:
