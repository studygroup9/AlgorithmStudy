class Solution {

    fun searchInsert(nums: IntArray, target: Int): Int {

        if(target < nums[0]) return 0

         return logic(0, nums.lastIndex, target, nums)

    }

    fun logic(left: Int, right: Int, target: Int, nums: IntArray): Int{

        if(left > right) {
            return left
        }
        val pivot = (left + right) / 2

        return when{
           nums[pivot] == target -> pivot
           nums[pivot] < target -> logic(pivot + 1, right, target, nums)
           nums[pivot] > target -> logic(left, pivot - 1, target, nums)
            else -> 0
        }

    }
}
