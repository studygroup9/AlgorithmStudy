// Search Insert Position
// https://leetcode.com/problems/search-insert-position/
// 68 ms	39.6 MB

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var searchInsert = function (nums, target) {
  let _target = target - 1;
  let lastIndex;

  if (nums.indexOf(target) !== -1) {
    return nums.indexOf(target);
  }

  if (target <= nums[0]) {
    return 0;
  }

  while (1) {
    lastIndex = nums.lastIndexOf(_target);

    if (lastIndex === -1) {
      _target--;
      continue;
    }

    return lastIndex + 1;
  }
};
