// 숫자 문자열과 영단어
// https://programmers.co.kr/learn/courses/30/lessons/81301

function solution(s) {
  const list = [
    ['zero', 0],
    ['one', 1],
    ['two', 2],
    ['three', 3],
    ['four', 4],
    ['five', 5],
    ['six', 6],
    ['seven', 7],
    ['eight', 8],
    ['nine', 9],
  ];

  return (
    list.reduce((acc, value, index) => {
      const reg = new RegExp(value[0], 'g');
      acc = acc.replace(reg, value[1]);
      return acc;
    }, s) * 1
  );
}

/**
테스트 1 〉	통과 (0.08ms, 30.4MB)
테스트 2 〉	통과 (0.10ms, 30.2MB)
테스트 3 〉	통과 (0.11ms, 30.2MB)
테스트 4 〉	통과 (0.08ms, 30.3MB)
테스트 5 〉	통과 (0.12ms, 30.2MB)
테스트 6 〉	통과 (0.11ms, 30.2MB)
테스트 7 〉	통과 (0.09ms, 30.3MB)
테스트 8 〉	통과 (0.12ms, 30.2MB)
테스트 9 〉	통과 (0.14ms, 30.3MB)
테스트 10 〉	통과 (0.07ms, 30.7MB)
 */
