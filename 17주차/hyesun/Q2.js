// Valid Parentheses
// https://leetcode.com/problems/valid-parentheses/
// 76 ms	39.2 MB

/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function (s) {
  let stack = [];
  const couple = ['(', ')', '{', '}', '[', ']'];
  s = s.split('');

  while (s.length > 0) {
    let value = s.shift();
    let place = couple.indexOf(value);

    if (place % 2 === 0) {
      stack.push(value);
      continue;
    }

    let stackLastItem = stack[stack.length - 1];
    if (couple.indexOf(stackLastItem) === place - 1) {
      stack.pop();
      continue;
    }

    stack.push(value);
  }

  return stack.length > 0 ? false : true;
};
