// Palindrome Number
// https://leetcode.com/problems/palindrome-number/
// 184 ms	48 MB

/**
 * @param {number} x
 * @return {boolean}
 */
var isPalindrome = function (x) {
  x = x.toString();

  if (x.charAt(0) === '-') {
    return false;
  }

  let start = 0;
  let end = x.length - 1;
  let loopEnd = Math.floor(x.length / 2);

  for (let i = 0; i <= loopEnd; i++) {
    if (x.charAt(start) !== x.charAt(end)) {
      return false;
    }

    start++;
    end--;
  }

  return true;
};
