package test;

public class W17_Q1 {
/*
 * 
 * Given an integer x, return true if x is palindrome integer.

	An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.
	
	 
	
	Example 1:
	
	Input: x = 121
	Output: true
	Example 2:
	
	Input: x = -121
	Output: false
	Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
	Example 3:
	
	Input: x = 10
	Output: false
	Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
	Example 4:
	
	Input: x = -101
	Output: false
 * 
 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isPalindrome(-121));
	}
	
	public static boolean isPalindrome(int x) {
		String a = Long.toString(x);
		StringBuffer sb = new StringBuffer(a);
		
		System.out.println(sb.reverse().toString());
		
		return x > -1 && Long.parseLong(sb.reverse().toString()) == x? true : false;
    }

}
