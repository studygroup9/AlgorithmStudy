package test;

import java.util.Stack;

public class W17_Q2 {
/*
 * 
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

	An input string is valid if:
	
	Open brackets must be closed by the same type of brackets.
	Open brackets must be closed in the correct order.
	 
	
	Example 1:
	
	Input: s = "()"
	Output: true
	Example 2:
	
	Input: s = "()[]{}"
	Output: true
	Example 3:
	
	Input: s = "(]"
	Output: false
	Example 4:
	
	Input: s = "([)]"
	Output: false
	Example 5:
	
	Input: s = "{[]}"
	Output: true
 * 
 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isValid("()"));
	}
	
	public static boolean isValid(String s) {
		char [] a = s.toCharArray();
		
		Stack<Character> st = new Stack<Character>();
		
		String b = "({[";
		
		boolean answer = true;
		
		for(int i = 0; i < a.length; i++) {
			if (!st.empty()) {
				if(select(st.lastElement()) == 'x') {
					answer = false;
					break;
				}else {
					if (select(st.lastElement()) == a[i]) {
						st.pop();
						continue;
					}else if(select(st.lastElement()) != a[i]) {
						if (b.indexOf(a[i]) == -1) {
							answer = false;
							break;
						}
							
					}
				}
			}
			
			st.add(a[i]);
		}
		
		if(!st.empty()) answer = false;
		
		return answer;
    }
	
	public static Character select(char a) {
		char b = 'x';
		
		switch (a) {
			case '(' :
				b = ')'; break;
			case '{' :
				b = '}'; break;
			case '[' :
				b = ']'; break;
			default :
				break;
		}
		
		return b;
	}

}
