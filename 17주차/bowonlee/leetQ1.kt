/**
 leetCode - Valid Parentheses
 */

 class Solution {
    fun isValid(s: String): Boolean {
       val stack: Stack<Char> = Stack()

        s.forEach {
            if(stack.isEmpty() || it == '{'|| it == '('|| it == '['){
                stack.push(it)
            }else{
                val peek = stack.peek()
                if(peek == '{'){
                    if(it != '}'){
                        return false
                    } else {
                        stack.pop()
                    }
                } else if(peek == '[') {
                    if(it != ']'){
                        return false
                    }else {
                        stack.pop()
                    }
                } else if(peek == '(') {
                    if(it != ')'){
                        return false
                    }else {
                        stack.pop()
                    }
                }
            }
        }
       return stack.isEmpty()
    }
}