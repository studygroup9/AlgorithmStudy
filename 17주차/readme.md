# **17주차 문제**

- 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.

1. [palindrome-number Lv 1: leetCode Easy](https://leetcode.com/problems/palindrome-number/)

2. [valid-parentheses : leetCode Easy](https://leetcode.com/problems/valid-parentheses/)

3. [메뉴 리뉴얼 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/72411)

4. [N으로 표현 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42895)

Lv3는 16주차와 동일합니다.

화이팅 :fire: 👍:construction_worker: 👍 :fire:
