//���� : Valid Parentheses
//Input: s = "()"
//Output: true

class Solution {
public:
    bool isValid(string s) {
    
      if (s.size() == 0) return true;

        stack<char> sk;
        
        for (int i = 0 ; i < s.length() ; i++ ){
            if (s[i] == '(' || s[i] == '{'|| s[i] == '['){
                sk.push(s[i]);    
            }else{
                
                if (sk.size() == 0) return false;
                 
                switch(s[i])
                {
                    case ')':
                        if (sk.top() == '('){
                            sk.pop();
                            break;
                        }else
                            return false;
                    case '}':
                        if (sk.top() == '{'){
                            sk.pop();
                            break;
                        }else
                            return false;
                    case ']':
                        if (sk.top() == '['){
                            sk.pop();
                            break;
                        }else
                            return false;
                    default:
                        break;
                }    
            } 
        }
        return (sk.size() == 0) ? true : false;
    }
};