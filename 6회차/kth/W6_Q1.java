package test;

import java.awt.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;
import javax.swing.text.html.HTMLDocument.Iterator;

public class W6_Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [] absolutes = {4,7,12};
		boolean [] signs = {true,false,true};
		
		solution(absolutes, signs);
		
	}
	
	/*
	 * 음양 더하기
	 * 어떤 정수들이 있습니다. 이 정수들의 절댓값을 차례대로 담은 정수 배열 absolutes와 이 정수들의 부호를 차례대로 담은 불리언 배열 signs가 매개변수로 주어집니다. 실제 정수들의 합을 구하여 return 하도록 solution 함수를 완성해주세요.
	 * */
	
	public static int solution(int[] absolutes, boolean[] signs) {
		int answer = 0;
        
		for(int i = 0; i < absolutes.length; i++) {
			if(signs[i] == true){
				answer += absolutes[i];
			}else {
				answer += absolutes[i] * -1;
			}
		}
		
		System.out.println(answer);
        return answer;
    }
	
}

