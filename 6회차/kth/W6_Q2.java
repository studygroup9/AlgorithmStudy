package test;

import java.util.Arrays;

public class W6_Q2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] lottos = {44, 1, 0, 0, 31, 25};
		
		int [] win_nums = {31, 10, 45, 1, 6, 19};
		
		solution(lottos, win_nums);
		
	}
	
	
	/*
	 * 로또의 최고 순위와 최저 순위

	 * */
	
	public static int[] solution(int[] lottos, int[] win_nums) {
		int[] answer = {0, 0};
		int zero = 0;
		for(int i = 0; i < win_nums.length; i++) {
			
			if(lottos[i] == 0) 
				zero++;
			
			for(int j = 0; j < win_nums.length; j++) {
				if(win_nums[i] == lottos[j]) {
					answer[0]++;
					answer[1]++;
				}
			}
		}
		
		answer[0] += zero;
		
		for(int i = 0; i < answer.length; i++) {
			switch (answer[i]) {
			case 6:
				answer[i] = 1;
				break;
			case 5:
				answer[i] = 2;
				break;
			case 4:
				answer[i] = 3;
				break;
			case 3:
				answer[i] = 4;
				break;
			case 2:
				answer[i] = 5;
				break;
			default:
				answer[i] = 6;
				break;
			}
		}
		
		
		
		System.out.println(Arrays.toString(answer));
		
        return answer;
    }
	
}

