/**
 * 로또 최소 경우의 수 최악의 경우의 수  
 */
class Solution {
    fun solution(lottos: IntArray, win_nums: IntArray): IntArray {
        var answer: IntArray = intArrayOf()
        var zeroCnt = 0

        var ok = lottos.filter {
            if(it!=0){
                it
                true
            } else{
                zeroCnt++
                false
            }
        }.count { win_nums.contains(it) }

        var max = 7 - ok - zeroCnt

        var min = 7 - ok

        if(min == 7) min = 6
        if(max == 7) max = 6


        answer  = intArrayOf(max, min )


        return answer

    }
}
