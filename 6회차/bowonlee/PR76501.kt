/**
 * 음양 더하기  
 */

class Solution {
    fun solution(absolutes: IntArray, signs: BooleanArray): Int {
        return absolutes.mapIndexed { index, i ->
            when(signs[index]){
                true -> i
                false -> i * -1
            }

        }.sumBy { it }
    }
}
