# **5주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [음양 더하기: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/76501)

2. [로또의 최고 순위와 최저 순위: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/77484)

3. [문자열 압축: Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/60057)

4. [단어 변환: Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/43163)

화이팅  :fire: 👍:construction_worker: 👍 :fire:   
