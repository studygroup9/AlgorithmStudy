package week6

import kotlin.math.min

/**
 *
 * 각 단어는 알파벳 소문자로만 이루어져 있습니다.
    각 단어의 길이는 3 이상 10 이하이며 모든 단어의 길이는 같습니다.
    words에는 3개 이상 50개 이하의 단어가 있으며 중복되는 단어는 없습니다.
    begin과 target은 같지 않습니다.
    변환할 수 없는 경우에는 0를 return 합니다.

 * */

/**
 * 재귀적으로 가능한 모든 경로를 탐색하는 방식을 사용한다.
 *
 * */
class PR43163ChangeWord {
    var min = 100
    var target = ""
    fun solution(begin: String, target: String, words: Array<String>): Int {
        var answer = 0
        this.target = target
        if(!words.contains(target)) return answer
        // target은 빼고 시작

        routine(words.toList(), begin, 0);


        return min
    }

    fun routine(words: List<String>,being: String, count:Int){

        if(being == this.target){
            min = min(count,min)
            return
        }
        words.forEach {

            var chk = check(being, it)
            if(chk){
                routine(words.filter { temp -> it != temp  }, it, count+1 )
            }
        }
    }

    fun check(a:String, b:String): Boolean{
        var cnt = 0
        for(i in 0 until a.length){
            if(a[i] !== b[i]){
                cnt++
            }
        }

        if(cnt == 1){
            return true
        }
        return false
    }
}

fun main(){

    val sol = PR43163ChangeWord()

    println(sol.solution("hit", "cog", arrayOf("hot", "dot", "dog", "lot", "log", "cog")))


}
