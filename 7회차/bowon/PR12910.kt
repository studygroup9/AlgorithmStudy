class temp {
    fun solution(arr: IntArray, divisor: Int): IntArray {
        var answer = intArrayOf()

//        arr.forEach {
//            if(it%divisor == 0){
//                answer += it
//            }
//        }
//        return if(answer.isEmpty()){
//            intArrayOf(-1)
//        } else{
//            answer.sortedArray()
//        }
        answer = arr.filter { it%divisor == 0 }.sorted().toIntArray()

        return if(answer.isEmpty()){
            intArrayOf(-1)
        } else{
            answer
        }

        /**
         * 배열방식
         * 테스트 1 〉	통과 (16.05ms, 57.6MB)
        테스트 2 〉	통과 (16.92ms, 58.1MB)
        테스트 3 〉	통과 (16.48ms, 57.8MB)
        테스트 4 〉	통과 (12.46ms, 57.7MB)
        테스트 5 〉	통과 (13.99ms, 57.4MB)
        테스트 6 〉	통과 (2.90ms, 60.2MB)
        테스트 7 〉	통과 (0.19ms, 55.4MB)
        테스트 8 〉	통과 (0.02ms, 54.2MB)
        테스트 9 〉	통과 (16.38ms, 57.7MB)
        테스트 10 〉	통과 (13.11ms, 57.5MB)
        테스트 11 〉	통과 (16.41ms, 57.5MB)
        테스트 12 〉	통과 (18.58ms, 57.5MB)
        테스트 13 〉	통과 (0.22ms, 55.1MB)
        테스트 14 〉	통과 (16.11ms, 57.7MB)
        테스트 15 〉	통과 (12.49ms, 58.1MB)
        테스트 16 〉	통과 (16.11ms, 57.6MB)
         *
         * */

        /**
         * Fillter 방식
         *테스트 1 〉	통과 (17.20ms, 58.5MB)
        테스트 2 〉	통과 (18.82ms, 58.1MB)
        테스트 3 〉	통과 (13.88ms, 57.8MB)
        테스트 4 〉	통과 (20.78ms, 58.2MB)
        테스트 5 〉	통과 (13.47ms, 58.6MB)
        테스트 6 〉	통과 (12.09ms, 61MB)
        테스트 7 〉	통과 (9.69ms, 56.2MB)
        테스트 8 〉	통과 (6.35ms, 55.3MB)
        테스트 9 〉	통과 (5.45ms, 55.6MB)
        테스트 10 〉	통과 (16.19ms, 58.3MB)
        테스트 11 〉	통과 (19.24ms, 58.4MB)
        테스트 12 〉	통과 (17.58ms, 58.8MB)
        테스트 13 〉	통과 (10.77ms, 55.8MB)
        테스트 14 〉	통과 (15.54ms, 58.1MB)
        테스트 15 〉	통과 (17.60ms, 58.3MB)
        테스트 16 〉	통과 (18.26ms, 58.5MB)
         * */


    }
    fun main(){

    }
}
