// 나누어 떨어지는 숫자 배열
// https://programmers.co.kr/learn/courses/30/lessons/12910

function solution(arr, divisor) {
  let answer = arr
    .filter((v) => v % divisor === 0)
    .sort((a, b) => (a < b ? -1 : 1));
  answer = answer.length === 0 ? [-1] : answer;
  return answer;
}

/**
테스트 1 〉	통과 (0.12ms, 30.1MB)
테스트 2 〉	통과 (0.09ms, 30.2MB)
테스트 3 〉	통과 (0.11ms, 30.2MB)
테스트 4 〉	통과 (0.11ms, 30.5MB)
테스트 5 〉	통과 (0.09ms, 30.4MB)
테스트 6 〉	통과 (2.74ms, 33.5MB)
테스트 7 〉	통과 (0.29ms, 30.4MB)
테스트 8 〉	통과 (0.07ms, 30MB)
테스트 9 〉	통과 (0.54ms, 30.2MB)
테스트 10 〉	통과 (0.27ms, 30.5MB)
테스트 11 〉	통과 (0.14ms, 30.1MB)
테스트 12 〉	통과 (0.16ms, 30.4MB)
테스트 13 〉	통과 (0.39ms, 30.4MB)
테스트 14 〉	통과 (0.29ms, 30.3MB)
테스트 15 〉	통과 (0.24ms, 30.6MB)
테스트 16 〉	통과 (0.13ms, 30.3MB)
 */
