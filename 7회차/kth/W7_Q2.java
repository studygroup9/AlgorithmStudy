package test;

import java.util.Arrays;

public class W7_Q2 {

	/*
	 * 문자열 압축
	 * 간단한 예로 "aabbaccc"의 경우 "2a2ba3c"(문자가 반복되지 않아 한번만 나타난 경우 1은 생략함)와 같이 표현할 수 있는데, 이러한 방식은 반복되는 문자가 적은 경우 압축률이 낮다는 단점이 있습니다. 예를 들면, "abcabcdede"와 같은 문자열은 전혀 압축되지 않습니다. "어피치"는 이러한 단점을 해결하기 위해 문자열을 1개 이상의 단위로 잘라서 압축하여 더 짧은 문자열로 표현할 수 있는지 방법을 찾아보려고 합니다.
	 * 예를 들어, "ababcdcdababcdcd"의 경우 문자를 1개 단위로 자르면 전혀 압축되지 않지만, 2개 단위로 잘라서 압축한다면 "2ab2cd2ab2cd"로 표현할 수 있습니다. 다른 방법으로 8개 단위로 잘라서 압축한다면 "2ababcdcd"로 표현할 수 있으며, 이때가 가장 짧게 압축하여 표현할 수 있는 방법입니다.
	 * 다른 예로, "abcabcdede"와 같은 경우, 문자를 2개 단위로 잘라서 압축하면 "abcabc2de"가 되지만, 3개 단위로 자른다면 "2abcdede"가 되어 3개 단위가 가장 짧은 압축 방법이 됩니다. 이때 3개 단위로 자르고 마지막에 남는 문자열은 그대로 붙여주면 됩니다.
	 * */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(solution("aabbaccc"));
		System.out.println(solution("ababcdcdababcdcd"));
		System.out.println(solution("abcabcdede"));
		System.out.println(solution("abcabcabcabcdededededede"));
		System.out.println(solution("xababcdcdababcdcd"));
		
	}
	
	public static int solution(String s) {
        int answer = s.length();
        
        for(int i = 1; i <= s.length()/2; i++) {
            int point = 0;
            int len = s.length();
        	
            while(point + i <= s.length()) {
            	String s1 = s.substring(point, point + i);
            	point += i;

            	int cnt = 0;
            	while(point + i <= s.length()) {
            		if(s1.equals(s.substring(point, point + i))) {
            			++cnt;
                		point += i;
            		}else {
            			break;
            		}
            		
            	}
            	if(cnt > 0) {
        			len -= i * cnt;
        			
        			if(cnt < 9) len += 1;
        			else if(cnt < 99) len += 2;
        			else if(cnt < 999) len += 3;
        			else if(cnt < 9999) len += 4;
        			else if(cnt < 99999) len += 5;
        			else len += 6;
        		
        		}
            }
        	answer = Math.min(answer, len);
        }
        
        return answer;
    }
	
}

