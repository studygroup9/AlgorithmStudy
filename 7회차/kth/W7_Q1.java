package test;

import java.util.ArrayList;
import java.util.Collections;

public class W7_Q1 {

	/*
	 * 나누어 떨어지는 숫자 배열
	 * 
	 * array의 각 element 중 divisor로 나누어 떨어지는 값을 오름차순으로 정렬한 배열을 반환하는 함수, solution을 작성해주세요.
	 * divisor로 나누어 떨어지는 element가 하나도 없다면 배열에 -1을 담아 반환하세요.
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [] arr = {5, 9, 7, 10};
		int divisor = 5;
		
		System.out.println(solution(arr, divisor));
		
	}
	
	public static ArrayList<Integer> solution(int[] arr, int divisor) {
        ArrayList<Integer> answer = new ArrayList<Integer>();
        
        for(int i = 0; i < arr.length; i++) {
        	if(arr[i]%divisor == 0)
        		answer.add(arr[i]);
        }
        
        if(answer.size() == 0)
        	answer.add(-1);
        
        Collections.sort(answer);
        
        return answer;
    }
	
}

