# **7주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [실패율 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/42889)

2. [나누어 떨어지는 숫자 배열: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12910)

3. [문자열 압축: Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/60057)

4. [단어 변환: Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/43163)

6회차 level2, level3 를 푼 사람이 없기 때문에 level2,3는 6주차 문제를 그대로 풀겠습니다~ 

화이팅  :fire: 👍:construction_worker: 👍 :fire:   
