// 문자열 내 p와 y의 개수
// https://programmers.co.kr/learn/courses/30/lessons/12916?language=javascript

function solution(s) {
  let regex = /p|y/gi;
  const excludeLength = s.replace(regex, '').length;

  regex = /p/gi;
  const pLength = s.replace(regex, '').length;

  regex = /y/gi;
  const yLength = s.replace(regex, '').length;

  if (s.length === excludeLength) {
    return true;
  }

  return pLength === yLength ? true : false;
}

/**

테스트 1 〉	통과 (0.32ms, 30.3MB)
테스트 2 〉	통과 (0.27ms, 30.1MB)
테스트 3 〉	통과 (0.19ms, 30MB)
테스트 4 〉	통과 (0.90ms, 30.1MB)
테스트 5 〉	통과 (0.18ms, 30MB)
테스트 6 〉	통과 (0.18ms, 29.8MB)
테스트 7 〉	통과 (0.19ms, 30MB)
테스트 8 〉	통과 (0.31ms, 30MB)
테스트 9 〉	통과 (0.31ms, 30.1MB)
테스트 10 〉	통과 (0.28ms, 30.1MB)
테스트 11 〉	통과 (0.34ms, 30.1MB)
테스트 12 〉	통과 (0.31ms, 30MB)
테스트 13 〉	통과 (0.20ms, 30MB)
테스트 14 〉	통과 (0.29ms, 30MB)
테스트 15 〉	통과 (0.32ms, 29.8MB)
테스트 16 〉	통과 (0.29ms, 30MB)
테스트 17 〉	통과 (0.35ms, 30.1MB)
테스트 18 〉	통과 (0.30ms, 30.1MB)
테스트 19 〉	통과 (0.20ms, 29.9MB)
테스트 20 〉	통과 (0.35ms, 29.7MB)
테스트 21 〉	통과 (0.29ms, 29.9MB)
테스트 22 〉	통과 (0.32ms, 30.1MB)
테스트 23 〉	통과 (0.22ms, 30.2MB)
테스트 24 〉	통과 (0.18ms, 29.9MB)
테스트 25 〉	통과 (0.23ms, 30MB)
테스트 26 〉	통과 (0.37ms, 30.1MB)
테스트 27 〉	통과 (0.38ms, 30MB)
테스트 28 〉	통과 (0.30ms, 30MB)
테스트 29 〉	통과 (0.30ms, 29.9MB)
테스트 30 〉	통과 (0.33ms, 29.8MB)
 */
