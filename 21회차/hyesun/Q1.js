// 상호 평가
// https://programmers.co.kr/learn/courses/30/lessons/83201

function solution(scores) {
  return scores
    .reduce((acc, row, row_i) => {
      acc.push(row.map((_, col_i) => scores[col_i][row_i]));
      return acc;
    }, [])
    .map((studentScore, mine) => {
      let studentScoreCopy = [...studentScore];
      if (studentScore.filter((v) => v === studentScore[mine]).length === 1) {
        studentScoreCopy.splice(mine, 1);
        if (
          !(
            !studentScore.some((vv) => vv > studentScore[mine]) ||
            !studentScore.some((vv) => vv < studentScore[mine])
          )
        ) {
          studentScoreCopy.splice(mine, 0, studentScore[mine]);
        }
      }

      let score = studentScoreCopy.reduce((total, value, index) => {
        if (index === studentScoreCopy.length - 1) {
          return (total + value) / studentScoreCopy.length;
        }
        return total + value;
      }, 0);

      return getGrade(score);
    })
    .join('');
}

function getGrade(score) {
  if (score >= 90) {
    return 'A';
  }

  if (score >= 80) {
    return 'B';
  }

  if (score >= 70) {
    return 'C';
  }

  if (score >= 50) {
    return 'D';
  }

  return 'F';
}

/**
테스트 1 〉	통과 (0.19ms, 30MB)
테스트 2 〉	통과 (0.21ms, 30.2MB)
테스트 3 〉	통과 (0.20ms, 29.7MB)
테스트 4 〉	통과 (0.19ms, 29.8MB)
테스트 5 〉	통과 (0.20ms, 30.1MB)
테스트 6 〉	통과 (0.19ms, 29.9MB)
테스트 7 〉	통과 (0.27ms, 30.2MB)
테스트 8 〉	통과 (0.20ms, 30.3MB)
테스트 9 〉	통과 (0.19ms, 30.3MB)
테스트 10 〉	통과 (0.25ms, 30MB)
테스트 11 〉	통과 (0.26ms, 30MB)
테스트 12 〉	통과 (0.20ms, 30.3MB)
테스트 13 〉	통과 (0.15ms, 30.1MB)
테스트 14 〉	통과 (0.19ms, 30.1MB)
테스트 15 〉	통과 (0.31ms, 29.9MB)
테스트 16 〉	통과 (0.21ms, 30.2MB)
테스트 17 〉	통과 (0.21ms, 30.2MB)
테스트 18 〉	통과 (0.22ms, 30.1MB)
테스트 19 〉	통과 (0.22ms, 30MB)
테스트 20 〉	통과 (0.24ms, 30MB)
테스트 21 〉	통과 (0.18ms, 30.1MB)
 */
