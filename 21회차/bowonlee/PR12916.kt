package week21;

public class PR12916 {
        boolean solution(String s) {
            boolean answer = true;

            int pCnt = 0;
            int yCnt = 0;

            // [실행] 버튼을 누르면 출력 값을 볼 수 있습니다.
            System.out.println("Hello Java");


            for (int i = 0; i <s.length() ; i++) {
                if(s.toLowerCase().toCharArray()[i] == 'p') {
                    pCnt++;
                } else {
                    yCnt++;
                }
            }
            return pCnt == yCnt;
        }

}
