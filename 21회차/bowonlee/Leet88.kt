package week21

import java.util.*

class Solution {
    fun merge(nums1: IntArray, m: Int, nums2: IntArray, n: Int): Unit {

        val priorityQueue= PriorityQueue<Int>()

        nums1.filter { it!=0 }.forEach {
            priorityQueue.add(it);
        }
        nums2.filter { it!=0 }.forEach {
            priorityQueue.add(it)
        }

        val zero = m - priorityQueue.size
        if(zero>0){
            for (i in 0 .. zero){
                priorityQueue.add(0)
            }
        }


        nums1.forEachIndexed { index, i ->
            nums1[index] = priorityQueue.poll()
        }

        return
    }
}