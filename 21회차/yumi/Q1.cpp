// 문제 : 문자열 내 p와 y의 개수

#include <string>
#include <iostream>
using namespace std;

bool solution(string s)
{
    bool answer = true;
    int num1 =0;
    int num2 =0;
    
    for(int i=0; i< s.size() ;i++){
        
        s[i] =toupper(s[i]);
        if (s[i] == 'P') { num1= num1 + 1; }  
        if (s[i] == 'Y') { num2= num2 + 1; }  
        
    } 
    
    if (num1 != num2) { answer = false; }

    return answer;
}