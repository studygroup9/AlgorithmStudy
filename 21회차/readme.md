# **20주차 문제**

- 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.

1. [88. Merge Sorted Array: leetCode Easy](https://leetcode.com/problems/merge-sorted-array/)

2. [94. Binary Tree Inorder Traversal: leetCode Easy](https://leetcode.com/problems/binary-tree-inorder-traversal/)

3. [상호 평가 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/83201)

4. [문자열 내 p와 y의 개수 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/12916)

5. [이진 변환 반복하기 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/70129)

6. [보석 쇼핑 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/67258)

화이팅 :fire: 👍:construction_worker: 👍 :fire:
