package test;

import java.util.Arrays;

public class W21_Q4 {

	/*
	 * 0과 1로 이루어진 어떤 문자열 x에 대한 이진 변환을 다음과 같이 정의합니다.
	 * x의 모든 0을 제거합니다.
	 * x의 길이를 c라고 하면, x를 "c를 2진법으로 표현한 문자열"로 바꿉니다.
	 * 예를 들어, x = "0111010"이라면, x에 이진 변환을 가하면 x = "0111010" -> "1111" -> "100" 이 됩니다.
	 * 0과 1로 이루어진 문자열 s가 매개변수로 주어집니다. 
	 * s가 "1"이 될 때까지 계속해서 s에 이진 변환을 가했을 때, 
	 * 이진 변환의 횟수와 변환 과정에서 제거된 모든 0의 개수를 각각 배열에 담아 return 하도록 solution 함수를 완성해주세요.
	 * */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		solution("110010101001");
	}
	public static int[] solution(String s) {
        int[] answer = new int[2];
        
        //문자열이 1일때까지 무한 루프
        while(!s.equals("1")) {
        	//0 개수 필터링
        	answer[1] += (int) s.chars().filter(c -> c == '0').count();
        	//루프 수 카운트
        	answer[0]++;
        	//0지움
        	s = s.replaceAll("0", "");
        	System.out.println(s);
        	//문자열 길이 2진수로 변환
        	s = Integer.toBinaryString(s.length());
        }
        
		System.out.println(Arrays.toString(answer));
		
        return answer;
    }

}
