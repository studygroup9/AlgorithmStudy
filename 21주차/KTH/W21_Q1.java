package test;

import java.util.Arrays;

public class W21_Q1 {
/*
 *   두 정수 배열을 부여 nums1하고 nums2, 정렬 비 감소하기 위해 , 두 개의 정수 m와 n의 요소 수를 나타내는, nums1그리고 nums2각각을.
 *   병합 nums1 과 nums2단일 배열로 정렬은 비 내림차순 . 
 * 
 *   입력: nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
 *   출력: [1,2,2,3,5,6]
 *   설명: 병합할 배열은 [1,2,3] 및 [2,5,6]입니다.
 *   병합 결과는 [ 1 , 2 ,2, 3 ,5,6] 이며 밑줄이 그어진 요소는 nums1 에서 가져옵니다.
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		merge(new int[] {1,2,3,0,0,0}, 3, new int[] {2,5,6}, 3);
	}
	public static void merge(int[] nums1, int m, int[] nums2, int n) {
		System.arraycopy(nums2, 0, nums1, m, n);

		Arrays.sort(nums1);
        System.out.println(Arrays.toString(nums1));
    }
}
