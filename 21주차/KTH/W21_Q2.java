package test;

public class W21_Q2 {
/*
 * 
 * 대학 교수인 당신은, 상호평가를 통하여 학생들이 제출한 과제물에 학점을 부여하려고 합니다.
 * 아래는 0번부터 4번까지 번호가 매겨진 5명의 학생들이 자신과 다른 학생의 과제를 평가한 점수표입니다.
 * 
 * scores = [[100,90,98,88,65],[50,45,99,85,77],[47,88,95,80,67],[61,57,100,80,65],[24,90,94,75,65]]
 * result = "FBABD"
 * 
 * */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		solution(new int [][] {{100,90,98,88,65},{50,45,99,85,77},{47,88,95,80,67},{61,57,100,80,65},{24,90,94,75,65}});
	}
	public static String solution(int[][] scores) {
        String answer = "";
        String [] avg = new String [] {"A", "B", "C", "D", "F"};
        int [] nums = new int[scores.length];
        
        for(int i = 0; i < scores.length; i++) {
        	int max = 0;
        	int min = 101;
        	int tmp = 0, sum = 0;
        	
        	//max, min 값 (중복안되게)
        	for(int j = 0; j < scores[i].length; j++) {
        		if (i != j) {
        			max = Math.max(scores[j][i], max);
            		min = Math.min(scores[j][i], min);
        		}
        		//합
        		sum += scores[j][i];
        	}
        	//자신이 최대 or 최대값일 경우 자기 자신 제외하고 평균 냄
        	if(scores[i][i] < min || scores[i][i] > max) {
        		sum -= scores[i][i];
        		answer += calc(sum/(scores[i].length - 1));
    		}else {
    			answer += calc(sum/(scores[i].length));
        	}
    		
        }
        
        System.out.println(answer);
        
        return answer;
    }
    
	//성적
    public static String calc(int n) {
		if(90 <= n) {
			return "A";
		}else if(80 <= n) {
			return "B";
		}else if(70 <= n) {
			return "C";
		}else if(50 <= n) {
			return "D";
		}else {
			return "F";
		}
	}

}
