# **15주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [x만큼 간격이 있는 n개의 숫자 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12954)

2. [직사각형 별찍기 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12969)

3. [괄호 회전하기 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/76502)

4. [N으로 표현 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42895)

Lv3는 14주차와 동일합니다.

화이팅  :fire: 👍:construction_worker: 👍 :fire: