import java.util.*
/** 유효한 괄호 */

class Solution {
    fun solution(s: String): Int {
                var answer: Int =  0



        for (i in 1 .. s.length ){
            // 이동시키기
            val str = s.drop(i) + s.take(i)
            val stack: Stack<Char> = Stack()

            str.forEach {
                if(stack.isEmpty() || it == '{'|| it == '('|| it == '['){
                    stack.push(it)
                }else{
                    val peek = stack.peek()
                    if(peek == '{'){
                        if(it != '}'){
                            return@forEach
                        } else {
                            stack.pop()
                        }
                    } else if(peek == '[') {
                        if(it != ']'){
                            return@forEach
                        }else {
                            stack.pop()
                        }
                    } else if(peek == '(') {
                        if(it != ')'){
                            return@forEach
                        }else {
                            stack.pop()
                        }
                    }
                }
            }
            if(stack.empty()) {
                answer++
            }
        }

        return answer
    }
}

/***
테스트 1 〉	통과 (100.64ms, 64.6MB)
테스트 2 〉	통과 (92.93ms, 63.9MB)
테스트 3 〉	통과 (94.58ms, 63.6MB)
테스트 4 〉	통과 (101.76ms, 63.7MB)
테스트 5 〉	통과 (108.88ms, 63.3MB)
테스트 6 〉	통과 (105.81ms, 63.3MB)
테스트 7 〉	통과 (93.81ms, 63.2MB)
테스트 8 〉	통과 (120.53ms, 63.6MB)
테스트 9 〉	통과 (114.24ms, 63.5MB)
테스트 10 〉	통과 (110.71ms, 63.3MB)
테스트 11 〉	통과 (108.38ms, 62.9MB)
테스트 12 〉	통과 (11.54ms, 56.1MB)
테스트 13 〉	통과 (12.03ms, 56.1MB)
테스트 14 〉	통과 (11.69ms, 56.4MB)
 */