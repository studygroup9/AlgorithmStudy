// x만큼 간격이 있는 n개의 숫자
// https://programmers.co.kr/learn/courses/30/lessons/12954

function solution(x, n) {
  let answer = [];
  for (let i = 1; i <= n; i++) {
    answer.push(i * x);
  }

  return answer;
}

/*
테스트 1 〉	통과 (0.05ms, 30MB)
테스트 2 〉	통과 (0.05ms, 30.2MB)
테스트 3 〉	통과 (0.08ms, 30.2MB)
테스트 4 〉	통과 (0.09ms, 30.3MB)
테스트 5 〉	통과 (0.10ms, 30.3MB)
테스트 6 〉	통과 (0.05ms, 30.1MB)
테스트 7 〉	통과 (0.13ms, 30.3MB)
테스트 8 〉	통과 (0.09ms, 30.2MB)
테스트 9 〉	통과 (0.12ms, 30MB)
테스트 10 〉	통과 (0.05ms, 30MB)
테스트 11 〉	통과 (0.09ms, 30.3MB)
테스트 12 〉	통과 (0.09ms, 30.1MB)
테스트 13 〉	통과 (0.15ms, 30.4MB)
테스트 14 〉	통과 (0.15ms, 30.3MB)
*/
