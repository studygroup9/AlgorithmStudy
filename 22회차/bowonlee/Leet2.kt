class Solution {
    fun addTwoNumbers(l1: ListNode?, l2: ListNode?): ListNode? {
        var _l1 = l1
        var _l2 = l2
        var node: ListNode? = ListNode(0)
        val result = node
        var sum = 0

        while (_l1 != null || _l2 != null || sum > 0) {
            if (_l1 != null) {
                sum += _l1.`val`
                _l1 = _l1.next
            }
            if (_l2 != null) {
                sum += _l2.`val`
                _l2 = _l2.next
            }
            node!!.next = ListNode(sum % 10)
            sum /= 10
            node = node.next
        }

        return result!!.next
    }
}
/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */