# **22회차 문제**

1. [100. Same Tree: leetCode Easy](https://leetcode.com/problems/same-tree/)

2. [94. Binary Tree Inorder Traversal: leetCode Easy](https://leetcode.com/problems/binary-tree-inorder-traversal/)

3. [2. Add Two Numbers: leetCode Medium](https://leetcode.com/problems/binary-tree-inorder-traversal/)

4. [직업군 추천하기 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/84325)

5. [3진법 뒤집기 : Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/68935)

6. [뉴스 클러스터링: Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/17677)

7. [보석 쇼핑 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/67258)

화이팅 :fire: 👍:construction_worker: 👍 :fire:
