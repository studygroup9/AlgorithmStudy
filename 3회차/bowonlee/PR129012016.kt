package week3

import java.time.LocalDate
import java.util.*

// 2016년 요일 구하기
/**
 *  테스트 1 〉	통과 (12.42ms, 57.1MB)
 *  테스트 2 〉	통과 (12.48ms, 56.5MB)
 *  테스트 3 〉	통과 (15.47ms, 56.2MB)
 *  테스트 4 〉	통과 (14.03ms, 56.2MB)
 *  테스트 5 〉	통과 (12.42ms, 57MB)
 *  테스트 6 〉	통과 (12.93ms, 56.4MB)
 *  테스트 7 〉	통과 (13.28ms, 56.3MB)
 *  테스트 8 〉	통과 (12.21ms, 56.3MB)
 *  테스트 9 〉	통과 (12.25ms, 56.3MB)
 *  테스트 10 〉	통과 (13.77ms, 56.2MB)
 *  테스트 11 〉	통과 (12.36ms, 56.6MB)
 *  테스트 12 〉	통과 (13.85ms, 56.6MB)
 *  테스트 13 〉	통과 (12.76ms, 55.8MB)
 *  테스트 14 〉	통과 (12.24ms, 56.7MB)
 *
 * */
class PR129012016 {

    fun solution(month: Int, day: Int): String {
        var answer = ""

        val date = LocalDate.of(2016, month, day)

        return date.dayOfWeek.name.take(3)
    }
}

fun main(){

    val sol = PR129012016()
    print(sol.solution(1,1))
}
//         val week = arrayOf("SUN","MON","TUE","WED","THU","FRI","SAT")