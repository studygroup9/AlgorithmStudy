package week3

import java.util.*

/**
 *
 *  1. 인쇄 대기목록의 가장 앞에 있는 문서(J)를 대기목록에서 꺼냅니다.
 *  2. 나머지 인쇄 대기목록에서 J보다 중요도가 높은 문서가 한 개라도 존재하면 J를 대기목록의 가장 마지막에 넣습니다.
 *  3. 그렇지 않으면 J를 인쇄합니다.
 *
 *
 *  실제 큐 순회 사용하여 해결
 *  Pair 타입을 사용하여 목표 값을 특정한다.
 * */

class PR42587Printer{

    fun solution(priorities: IntArray, location: Int): Int{
        var answer = 0
        var max = priorities.max()


        val que: Queue<Pair<Boolean,Int>> = LinkedList<Pair<Boolean,Int>>()


        priorities.forEachIndexed { index, i ->
            if(index == location){
                que.add(Pair(true, i))
            } else{
                que.add(Pair(false, i))
            }
        }

        while (true){
            val top = que.poll()
            if(top.second == max){
                answer++
                if(top.first) {
                    return answer
                } else {
                  priorities.set(priorities.indexOf(top.second),0)
                  max = priorities.max()
                }
            } else {
                que.add(top)
            }
        }


    }
}

fun main(){
    val sol = PR42587Printer()

    print(sol.solution(intArrayOf(1,1,1,9,1,1), 3))


}

/**
 *   테스트 1 〉	통과 (16.35ms, 57.9MB)
 *   테스트 2 〉	통과 (17.65ms, 57.5MB)
 *   테스트 3 〉	통과 (16.00ms, 57.5MB)
 *   테스트 4 〉	통과 (17.36ms, 58.2MB)
 *   테스트 5 〉	통과 (11.85ms, 57.2MB)
 *   테스트 6 〉	통과 (18.62ms, 57.2MB)
 *   테스트 7 〉	통과 (16.37ms, 58.5MB)
 *   테스트 8 〉	통과 (18.86ms, 57.1MB)
 *   테스트 9 〉	통과 (16.87ms, 58.5MB)
 *   테스트 10 〉	통과 (17.57ms, 58.4MB)
 *   테스트 11 〉	통과 (12.47ms, 56.9MB)
 *   테스트 12 〉	통과 (16.54ms, 57.7MB)
 *   테스트 13 〉	통과 (17.69ms, 58MB)
 *   테스트 14 〉	통과 (16.57ms, 57.9MB)
 *   테스트 15 〉	통과 (16.84ms, 57.9MB)
 *   테스트 16 〉	통과 (15.74ms, 57.8MB)
 *   테스트 17 〉	통과 (17.05ms, 57.9MB)
 *   테스트 18 〉	통과 (16.60ms, 57.9MB)
 *   테스트 19 〉	통과 (17.53ms, 57.6MB)
 *   테스트 20 〉	통과 (12.82ms, 58MB)
 *
 * */