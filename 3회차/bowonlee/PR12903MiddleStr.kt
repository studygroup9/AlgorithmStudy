/**
 *
 *   테스트 1 〉	통과 (0.05ms, 54.8MB)
 *   테스트 2 〉	통과 (0.05ms, 54.4MB)
 *   테스트 3 〉	통과 (0.04ms, 54.7MB)
 *   테스트 4 〉	통과 (0.06ms, 55.2MB)
 *   테스트 5 〉	통과 (0.06ms, 54.4MB)
 *   테스트 6 〉	통과 (0.05ms, 56.4MB)
 *   테스트 7 〉	통과 (0.06ms, 55.3MB)
 *   테스트 8 〉	통과 (0.05ms, 55.3MB)
 *   테스트 9 〉	통과 (0.05ms, 54.5MB)
 *   테스트 10 〉	통과 (0.05ms, 55.5MB)
 *   테스트 11 〉	통과 (0.05ms, 54.7MB)
 *   테스트 12 〉	통과 (0.05ms, 55.2MB)
 *   테스트 13 〉	통과 (0.06ms, 54.8MB)
 *   테스트 14 〉	통과 (0.05ms, 55.1MB)
 *   테스트 15 〉	통과 (0.05ms, 55.2MB)
 *   테스트 16 〉	통과 (0.07ms, 54.5MB)
 *
 * */

class PR12903MiddleStr {
    fun solution(s: String): String {
        var answer = ""

        if(s.length % 2 == 0){
            answer += s[s.length/2 -1]
            answer += s[s.length/2 ]
        } else {
            answer += s[s.length/2]
        }


        return answer
    }
}

fun main(){
    val sol =PR12903MiddleStr()
    println(sol.solution("abcde"))
    println(sol.solution("qwer"))
}