# **3주차 문제**

1. [2016년: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12901)

2. [가운데 글자 가져오기: Programmers Lv1](https://programmers.co.kr/learn/courses/30/lessons/12903)

3. [프린터: Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42587)

4. [네트워크: Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/43162)

화이팅👍👍👍

LV3 문제 중 가장 많은 사람들이 풀어본 문제로 선택했습니다~ 
