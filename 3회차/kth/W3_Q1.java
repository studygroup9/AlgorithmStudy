package test;

public class W3_Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solution("abcde"));
		System.out.println(solution("awed"));
	}
	
	static String solution(String s) {
		String answer = "";
	
		int len = s.length();
        
		System.out.println(len/2);
		
		if(len%2 == 0){//짝수
            answer = s.substring(len/2-1, len/2+1);
        }else{//홀수
            answer = s.substring(len/2, len/2+1);
        }
		
		return answer;
	}

}
