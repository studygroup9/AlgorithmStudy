package test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.stream.Collectors;

public class W3_Q3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		int [] priorities = {2, 1, 3, 2};
//		int location = 2;
		int [] priorities = {1, 1, 9, 1, 1, 1};
		int location = 0;
		System.out.println(solution(priorities, location));
	}
	
	
	public static int solution(int[] priorities, int location) {
        int answer = 0;
        
        Queue<Integer> queue = new LinkedList<Integer>();
        
        for(int i : priorities)
        	queue.add(i);
        
        Arrays.sort(priorities);			//최대값 구하기 위해 정렬
        int len = priorities.length-1;		//최대값 위치
        
        while(!queue.isEmpty()) {
        	int i = queue.poll();		//첫번 째 큐 확인
        	if(i == priorities[len - answer]) {
        		answer++;				//순서 증가
        		location--;				//위치 차감
        		if(location < 0)		
        			break;				//탈출
        	}else {
        		queue.add(i);			//다시 추가
        		location--;				//위치 차감
        		if(location < 0)		//위치 갱신
        			location = queue.size()-1;
        	}
        }
        
        return answer;
    }
	
	
	
}
