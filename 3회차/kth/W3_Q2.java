package test;

import java.util.Calendar;

public class W3_Q2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1:일요일, 2:월요일, ...., 7: 토요일
		//SUN,MON,TUE,WED,THU,FRI,SAT
		
		System.out.println(solution(5, 24));
	}
	
	static String solution(int a, int b) {
		String answer = "";
		String [] week = { "SUN","MON","TUE","WED","THU","FRI","SAT" };
		
		Calendar cal = Calendar.getInstance();
        cal.set(2016 ,a-1 ,b);
		int w = cal.get(Calendar.DAY_OF_WEEK)-1;
		answer = week[w];
		
		return answer;
	}

}
