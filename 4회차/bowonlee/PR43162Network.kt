package week4

class PR43162Network {
    // 네트워크에 한번 속하면 해당 노드부터 시작하는 네트워크는 검사 할 필요가 없다.
    // 네트워크에 한번이라도 속한 노드들 set애 저장
    val network: HashSet<Int> = hashSetOf()
    private lateinit var nodes: Array<IntArray>
    fun solution(n: Int, computers: Array<IntArray>): Int {
        var answer = 0
        setNodes(computers);

        computers.forEachIndexed { index, i ->
        // 기존 네트워크에 포함 된 적이 없는 컴퓨터들 부터 시작하는 네트워크 구성
            if (!network.contains(index)) {
                answer++
                this.connect(i, index)
            }
        }

        return answer
    }

    fun setNodes(computers: Array<IntArray>) {
        this.nodes = computers
    }

    // 해당 노드를 네트워크에 포함 시키면서 시작한다.
    // 무한루프를 방지하기 위해 자기 자신의 노드는 재귀하지 않고 넘어간다.
    fun connect(row: IntArray, num: Int) {
        if (!network.contains(num)) {
            network.add(num)
            row.forEachIndexed { index, i ->
                if (i == 1) {
                    if (num == index) return@forEachIndexed
                    connect(nodes[index], index)
                }
            }

        }
    }
}

fun main() {

    val sol = PR43162Network()

//    println (sol.solution(3, arrayOf(intArrayOf(1,1,0), intArrayOf(1,1,0), intArrayOf(0,0,1))))
    println(sol.solution(3, arrayOf(intArrayOf(1, 1, 0), intArrayOf(1, 1, 1), intArrayOf(0, 1, 1))))

}

/**
 *
 *
 * 네트워크란 컴퓨터 상호 간에 정보를 교환할 수 있도록 연결된 형태를 의미합니다. 예를 들어,
 * 컴퓨터 A와 컴퓨터 B가 직접적으로 연결되어있고,
 * 컴퓨터 B와 컴퓨터 C가 직접적으로 연결되어 있을 때 컴퓨터 A와 컴퓨터 C도 간접적으로 연결되어 정보를
 * 교환할 수 있습니다. 따라서 컴퓨터 A, B, C는 모두 같은 네트워크 상에 있다고 할 수 있습니다.
 * 컴퓨터의 개수 n, 연결에 대한 정보가 담긴 2차원 배열 computers가 매개변수로 주어질 때,
 * 네트워크의 개수를 return 하도록 solution 함수를 작성하시오.
 * */

/**
 *
 *   테스트 1 〉	통과 (0.05ms, 54.7MB)
 *   테스트 2 〉	통과 (0.05ms, 55.8MB)
 *   테스트 3 〉	통과 (0.11ms, 55.5MB)
 *   테스트 4 〉	통과 (0.11ms, 55.2MB)
 *   테스트 5 〉	통과 (0.03ms, 54.6MB)
 *   테스트 6 〉	통과 (2.13ms, 55MB)
 *   테스트 7 〉	통과 (0.06ms, 54.7MB)
 *   테스트 8 〉	통과 (0.25ms, 54.8MB)
 *   테스트 9 〉	통과 (0.17ms, 55.1MB)
 *   테스트 10 〉	통과 (0.15ms, 54.8MB)
 *   테스트 11 〉	통과 (1.94ms, 55.4MB)
 *   테스트 12 〉	통과 (0.71ms, 55MB)
 *   테스트 13 〉	통과 (0.34ms, 55.1MB)
 *
 * */