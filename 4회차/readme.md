# **4주차 문제**

4주차는 3주차의 Level2, Level3를 이어서 풀도록 합니다.<br>
level1은 한문제만 추가합니다. 

* 3,4주차 머지 리퀘스트는 함께 진행합니다. 


1. [3진법 뒤집기: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/68935)

2. [프린터: Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42587)

3. [네트워크: Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/43162)

화이팅  :fire: 👍:construction_worker: 👍 :fire:   