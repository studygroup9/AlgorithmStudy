package week11

import kotlin.math.sqrt

class PR42842 {
    fun solution(brown: Int, yellow: Int): IntArray {
        var answer = intArrayOf()
        val sum = brown + yellow

        for(i in 3 .. sum/2 ){

            // 답이 항상 있다면 반드시 절반 이전에 반환..
            if(sum%i == 0){
                if((i-2 + sum/i) == brown/2){
                    return intArrayOf(i, sum/i)
                }
            }

        }

        return answer
    }
}

fun main(){

    val sol = PR42842()
    println(sol.solution(10,2).toList())
}

/**
테스트 1 〉	통과 (0.02ms, 54.6MB)
테스트 2 〉	통과 (0.02ms, 55.5MB)
테스트 3 〉	통과 (0.04ms, 54.8MB)
테스트 4 〉	통과 (0.02ms, 54.6MB)
테스트 5 〉	통과 (0.01ms, 55.5MB)
테스트 6 〉	통과 (0.03ms, 54.4MB)
테스트 7 〉	통과 (0.03ms, 54.4MB)
테스트 8 〉	통과 (0.04ms, 55MB)
테스트 9 〉	통과 (0.03ms, 55.1MB)
테스트 10 〉	통과 (0.04ms, 54.5MB)
테스트 11 〉	통과 (0.01ms, 55.2MB)
테스트 12 〉	통과 (0.01ms, 54.5MB)
테스트 13 〉	통과 (0.02ms, 54.8MB)

 */