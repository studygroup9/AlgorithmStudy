# **11주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [서울에서 김서방 찾기 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12919)

2. [약수의 합 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12928)

3. [카펫 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42842)

4. [디스크 컨트롤러 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42627)

디스크 컨트롤러는 8,9,10 주차와 동일합니다~

화이팅  :fire: 👍:construction_worker: 👍 :fire: