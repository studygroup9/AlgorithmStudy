package test;

import java.util.ArrayList;
import java.util.Arrays;

public class W11_Q3 {

	/*
	 * 카펫
	 * 
	 * 갈색 격자의 수 brown은 8 이상 5,000 이하인 자연수입니다.
	 * 노란색 격자의 수 yellow는 1 이상 2,000,000 이하인 자연수입니다.
	 * 카펫의 가로 길이는 세로 길이와 같거나, 세로 길이보다 깁니다.
	 * 
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 50	;  
		int b = 22	;
		//[12, 6]
		System.out.println(Arrays.toString(solution(a, b)));
	}
	
	public static int[] solution(int brown, int yellow) {
        int [] answer = {0,0};
        
        ArrayList<Integer> list = new ArrayList<Integer>();
        
        int tmp = brown + yellow;

        for (int i = tmp; i >= 1; i--) {
            if (tmp % i == 0) {
            	list.add(i);
            }        
        }

        int a = list.size() % 2;	//약수 구한 배열 개수가 홀수인지 짝수인지 
        int b = list.size() / 2;
        int j = b;
        
        
        if(a == 1) {	//홀수라면 가운데 수 삽입
        	answer[0] = list.get(b);
        	answer[1] = list.get(b);
        }

        
    	for(int i = b-1; i >= 0; i--) {

    		if ((list.get(i)+list.get(j)-2) * 2 == brown) {
    			answer[0] = list.get(i);
    	    	answer[1] = list.get(j);
    	    	break;
    		}
        	
        	j++;
        }
        
        return answer;
    }

}

