package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Q1_W1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solution("...!@BaT#*..y.abcdefghijkl0m"));	
		System.out.println(solution("z-+.^."));
		System.out.println(solution("=.="));
		System.out.println(solution("123_.def"));
		System.out.println(solution("abcdefghijklmn.p"));
		System.out.println("----------------------------------------");
		System.out.println(solution2("...!@BaT#*..y.abcdefghijkl0m"));	
		System.out.println(solution2("z-+.^."));
		System.out.println(solution2("=.="));
		System.out.println(solution2("123_.def"));
		System.out.println(solution2("abcdefghijklmn.p"));
	}

	public static String solution2(String new_id) {
        String answer = "";
        String temp = new_id.toLowerCase();
        temp = temp.replaceAll("[^-_.a-z0-9]",""); //-_.a-z0-9를 제외한 물자 삭제
        temp = temp.replaceAll("[.]{2,}","."); // . 가 2번 이상 나오면 .으로 치환
        temp = temp.replaceAll("^[.]|[.]$",""); //^ 는 행시작, $는 행 끝의 .를 삭제
        if(temp.equals(""))	//
            temp+="a";
        if(temp.length() >=16){
            temp = temp.substring(0,15);
            temp=temp.replaceAll("^[.]|[.]$","");
        }
        if(temp.length()<=2)
            while(temp.length()<3)
                temp+=temp.charAt(temp.length()-1);
        answer=temp;
        return answer;
    }
	
	public static String regEx(String val){
        String result = "";
        String regEx = "[~!@#[$]%^&[*]\\(\\)=[+]\\{\\}\\[\\]:?,<>/]";

        Pattern pattern = Pattern.compile(regEx);//-_. 가능
        Matcher m;
        result = val.replaceAll(regEx, "");
        return result;
    }
    
    public static String pointTran(String new_id) {
    	
    	while(true) {
    		if(new_id.indexOf("..") < 0)
    			break;
    		else
    			new_id = new_id.replace("..", ".");
    	}
    	return new_id;
    }
    
    public static String stringSplit(String new_id) {
    	
    	if (new_id.length() > 14)
    		new_id = new_id.substring(0, 15);
    	
    	int i = 0;
    	
    	while(new_id.length() > 14) {
    			
    		if(new_id.charAt(14-i) == '.') {
        		new_id = new_id.substring(0, 13-i);
        		i++;
        	}else{
        		break;
        	}
    	}
    	
    	
    	return new_id;
    }
    
    public static String pointRomve(String new_id) {
    	String result = "";
    	
    	int j = 0;
    	while(true) {
    		if(j == new_id.length()) {
    			break;
    		}
    		if(new_id.charAt(j) == '.')
				j++;
    		else 
    			break;
    	}
    	result = new_id.substring(j, new_id.length());
    	
    	if(result.length() != 0 && result.charAt(result.length()-1) == '.') {
    		result = result.substring(0, result.length() - 1);
		}
    	
    	return result;
    }
    
    public static String lengthAdd(String new_id) {
    	
    	if(new_id.length() < 3) {
//    		System.out.println("lengthAdd: " + new_id.length());
    		while(true) {
    			if(new_id.equals("")) {
    				new_id += "a";
    			} else if(new_id.length() >= 3) {
    				break;
    			}else {
    				new_id += Character.toString(new_id.charAt(new_id.length()-1));
    			}
    		}
        }
    	return new_id;
    }

    public static String solution(String new_id) {
        String answer = new_id;
        //대->소문자 변환
        answer = answer.toLowerCase();
        //정규식 특수문자 제거
        answer = regEx(answer);
        //연속된 .. -> . 변환
        answer = pointTran(answer);
        //앞뒤 . 제거 
        answer = pointRomve(answer);
        //빈 문자열 -> "a" 변환
        answer = answer.replaceAll(" ", "a");
        //15자 이상 제거
        answer = stringSplit(answer);
        //2자 이하 끝자리 반복 붙여넣기
        answer = lengthAdd(answer);
        
        return answer;
    }
	
	
}
