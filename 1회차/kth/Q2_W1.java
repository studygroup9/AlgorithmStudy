package test;

import java.util.Arrays;

public class Q2_W1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(reverseStr("Zbcdefg"));
		System.out.println(asciiCode("Zbcdefg"));
		
	}
	public static String reverseStr(String s){
       char[] sol = s.toCharArray();
       Arrays.sort(sol);
       return new StringBuilder(new String(sol)).reverse().toString();
    }

   private static String asciiCode(String s) {
      String answer = "";
      char [] arr =  s.toCharArray();
      
   
      for(int i=0; i<arr.length; i++) {
         for(int j=i+1; j<arr.length; j++) {
            if((int) arr[i] < (int) arr[j]) { //내림차순
               char tmp = arr[i];
               arr[i] = arr[j];
               arr[j] = tmp;
            }
         }
      }
      
      answer = new String(arr);
      return answer;
   }

}
