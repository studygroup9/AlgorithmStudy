package test;

public class Q4_W1 {

   public static void main(String[] args) {
      // TODO Auto-generated method stub

      int N = 5;
      int M = 21;
      int c[] = {5, 6, 7, 8, 9};
      
      System.out.println(blackjack(N, M, c));
      N = 10;
      M = 500;
      int d [] = {93, 181, 245, 214, 315, 36, 185, 138, 216, 295};
      System.out.println(blackjack(N, M, d));
      
   }
   public static int blackjack(int a, int max, int [] c) {
      int N = c.length;
      int sum = 0;
      int result = 0;
      
      for (int i = 0; i < N-2; i++) {
         for(int j = i+1; j < N-1; j++) {
            for(int k = j+1; k < N; k++) {
               sum = c[i] + c[j] + c[k];
               if(sum <= max && result < sum){
                  result = sum;
               }
               
            }
         }
      }
      
      
      return result;
   }


}