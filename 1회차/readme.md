# **1주차 문제** 

1. [신규 아이디 추천 : Programmers](https://programmers.co.kr/learn/courses/30/lessons/72410)

2. [문자열 내림차순으로 배치하기: Programmers](https://programmers.co.kr/learn/courses/30/lessons/12917)

3. [이상한 문자 만들기: Programmers](https://programmers.co.kr/learn/courses/30/lessons/12930)

4. [블랙잭: BEAKJOON](https://www.acmicpc.net/problem/2798)

화이팅👍