package week1

import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.max

class BlackJack{
    lateinit var numArray: IntArray
    var goal = 0
    var result = 0;

    constructor(numArray: IntArray, goal: Int) {
        this.numArray = numArray
        this.goal = goal
    }


    fun recursionFunc(index: Int, sumCnt:Int, sum: Int){


        if(sumCnt == 3 && sum <= goal){
               result = max(sum,result)
            return
        }
        if(index >= numArray.size || sumCnt >3 || sum>goal) return


        recursionFunc(index+1,sumCnt+1,sum+numArray[index])
        recursionFunc(index+1,sumCnt,sum )
    }

}


fun main() = with(BufferedReader(InputStreamReader(System.`in`))) {


    val inputSchema = readLine().split(' ').map { it.toInt() }

    val unputCnt = inputSchema[0]
    val goalNum = inputSchema[1]

    val numArray = readLine().split(' ').map { it.toInt() }


    val solution = BlackJack(numArray.toIntArray(), goalNum)

    solution.recursionFunc(0,0,0);

    print(solution.result)


    return@with

}

