class Solution {
    fun solution(str: String): String {
        var answer = ""
        str.split(' ')
        .forEach { s ->
             s.forEachIndexed { index, c ->
                if(index%2 == 0){
                    answer += c.toUpperCase()

                } else{
                    answer += c.toLowerCase()
                }
            }
            answer += ' '
        }
        return  answer.dropLast(1)
    }
}