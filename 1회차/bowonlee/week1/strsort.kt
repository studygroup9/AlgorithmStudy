package week1

import java.util.*


fun solution(s: String): String {
    var answer = ""

    val myCustomComparator =  Comparator<Char> { a, b ->
        when {
            (a.isUpperCase() && b.isLowerCase()) -> 1
            (a.isLowerCase() && b.isUpperCase()) -> -1
            else -> 1
        }
    }


    val strList = s.toList().sortedByDescending { it.toInt() }.sortedWith(myCustomComparator)


    return strList.joinToString("")

}


fun main(){
    val result = solution("ZbcdAefg");

    print(result);
}
/**
 * s는 영문 대소문자로만 구성되어 있으며, 대문자는 소문자보다 작은 것으로 간주합니다.
 * */