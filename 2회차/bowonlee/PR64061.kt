package week2
import java.util.*

// 인형뽑기

class PR64061 {

    fun solution(board: Array<IntArray>, moves: IntArray): Int{
        var answer = 0
        var stack = Stack<Int>()
        stack.push(-1)

        moves.forEach { move ->
            board.forEachIndexed { index, ints ->
                if(ints[move-1] != 0){

                    if(stack.peek() == ints[move-1]){
                        stack.pop()
                        answer+=2
                    } else{

                        stack.push(ints[move-1])
                    }

                    ints.set(move-1,0)
                    return@forEach
                }


            }
        }
        return answer


    }
}

fun main(){

    val sol = PR64061()
//[[0,0,0,0,0],[0,0,1,0,3],[0,2,5,0,1],[4,2,4,4,2],[3,5,1,3,1]]
    sol.solution(
        arrayOf(intArrayOf(0,0,0,0,0), intArrayOf(0,0,1,0,3), intArrayOf(0,2,5,0,1), intArrayOf(4,2,4,4,2), intArrayOf(3,5,1,3,1)),
        intArrayOf(1,5,3,5,1,2,1,4)
    )
}