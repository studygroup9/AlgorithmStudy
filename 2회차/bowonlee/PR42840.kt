package week2

// 모의고사

class PR42840 {

    fun solution(answers: IntArray): IntArray{
        var answer = mutableListOf<Int>()

        val pattern1 = intArrayOf(1,2,3,4,5)
        val pattern2 = intArrayOf(2,1,2,3,2,4,2,5)
        val pattern3 = intArrayOf(3,3,1,1,2,2,4,4,5,5)

        val patterns = arrayOf(pattern1,pattern2,pattern3)

        val rightCntArray = intArrayOf(0,0,0)

        patterns.forEachIndexed { patternIndex, pattern ->
            answers.forEachIndexed { answerIndex, i ->
                if(i == pattern[answerIndex%pattern.lastIndex+1]){
                    rightCntArray[patternIndex]++
                }
            }
        }

        rightCntArray.forEachIndexed { index, i ->
            if(rightCntArray.max() == i) {
                answer.add(index+1)
            }
        }

        return answer.toIntArray()


    }
}

fun main(){

    val sol = PR42840()

    print(sol.solution(intArrayOf(1,2,5,5,2)).toList())

}