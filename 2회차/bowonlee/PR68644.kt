package week2



class PR68644 {

    fun solution(numbers: IntArray): IntArray{
        val numSet = mutableSetOf<Int>()
        for(i in 0 .. numbers.lastIndex - 1 ){
            for(j in i+1 .. numbers.lastIndex ){
                numSet.add(numbers[i] + numbers[j])
            }
        }

        return numSet.sorted().toIntArray()
    }
}

fun main(){
    val sol = PR68644()

    println(sol.solution(intArrayOf(2,1,3,4,1)).toList())
    println(sol.solution(intArrayOf(5,0,2,7)).toList())

}