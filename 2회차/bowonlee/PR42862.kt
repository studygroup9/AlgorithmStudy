package week2

// 체육복

class PR42862 {

    fun solution(n: Int, lost: IntArray, reserve: IntArray): Int {
        var answer = 0


        // kotlin 의 연산자 오버라이딩
        // 합집합, 차집합 교집합을 내장 함수로 구할 수 있다,
        // MutableList와 List 타입이 엄격하게 구분된다.
        var filteredReserve = (reserve.toList() - lost.toList()).toMutableList()


        var filteredLost = (lost.toList() - reserve.toList()).toMutableList()


        filteredLost.forEach { i ->
            if(filteredReserve.contains(i-1)){
                filteredReserve.removeAt(filteredReserve.indexOf(i-1))
                return@forEach
            }
            if(filteredReserve.contains(i+1)){
                filteredReserve.removeAt(filteredReserve.indexOf(i+1))
                return@forEach
            }
            answer++

        }


        return n-answer
    }
}

fun main() {

    val sol = PR42862()
    println(sol.solution(5, intArrayOf(2,4), intArrayOf(1,3,5)))
    println(sol.solution(5, intArrayOf(2,4), intArrayOf(3)))
    println(sol.solution(3, intArrayOf(3), intArrayOf(1)))
    println(sol.solution(7, intArrayOf(2,3,4), intArrayOf(1,2,3,6)))
    println(sol.solution(4, intArrayOf(1,2,3), intArrayOf(1,2,3,5)))

}