package test;

import java.util.HashSet;

public class Q1_W2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] i = {2, 3, 4};
		int [] j = {1, 3, 5};
		System.out.println(solution(5,i,j));
//		System.out.println(solution3(5,i,j));
		int [] i1 = {2, 4};
		int [] j1 = {3};
		System.out.println(solution(5,i1,j1));
//		System.out.println(solution3(5,i1,j1));
	}
	public static int solution(int n, int[] lost, int[] reserve) {
		int answer = n - lost.length;
		HashSet<Integer> k = new HashSet<Integer>();
		
		for(int l : reserve) {
			k.add(l);
		}
        
        for(int j = 0; j < lost.length; j++){
            if(k.contains(lost[j])) {
				k.remove(lost[j]);
				answer++;
                lost[j] = -1;
            }
        }
		
		for(int i = 0; i < lost.length; i++) {
            if(k.contains(lost[i]-1)) {
                k.remove(lost[i]-1);
                answer++;
            }else if(k.contains(lost[i]+1)) {
                k.remove(lost[i]+1);
                answer++;
            }
		}
		
		return answer;
	}
	
	public static int solution2(int n, int[] lost, int[] reserve) {
        int[] people = new int[n];
        int answer = n;

        for (int l : lost) {
        	people[l-1]--;
        }
        
        
        for (int r : reserve) {
        	people[r-1]++;
        }
            
        
        
        for (int i = 0; i < people.length; i++) {
            if(people[i] == -1) {
                if(i-1>=0 && people[i-1] == 1) {
                    people[i]++;
                    people[i-1]--;
                }else if(i+1< people.length && people[i+1] == 1) {
                    people[i]++;
                    people[i+1]--;
                }else 
                    answer--;
            }
        }
        return answer;
    }
	
}
