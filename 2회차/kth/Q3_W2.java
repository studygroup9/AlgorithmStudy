package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class W2_Q3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(solution(new int[] {3, 3, 1, 1, 1, 1, 2, 3, 4, 5}));
	}
	static ArrayList<Integer> solution(int[] answers) {
        int [] s1 = {1, 2, 3, 4, 5};
		int [] s2 = {2, 1, 2, 3, 2, 4, 2, 5};
		int [] s3 = {3, 3, 1, 1, 2, 2, 4, 4, 5, 5};
		
		HashMap<Integer, Integer> map = new HashMap<>();
		map.put(1, 0);
		map.put(2, 0);
		map.put(3, 0);
        
		for(int i = 0; i < answers.length; i++) {
			if(s1[i%s1.length] == answers[i]) {
				if(map.containsKey(1)) {
					map.replace(1, map.get(1) + 1);
				}
			}
			if(s2[i%s2.length] == answers[i]) {
				if(map.containsKey(2)) { 
					map.replace(2, map.get(2) + 1);
				}
			}
			if(s3[i%s3.length] == answers[i]) {
				if(map.containsKey(3)) { 
					map.replace(3, map.get(3) + 1);
				}
			}
		}
		
		int maxValueInMap = (Collections.max(map.values()));

		for(int i = 0; i <= map.size(); i++) {
			if(map.containsKey(i+1) && map.get(i+1) != maxValueInMap )
				map.remove(i+1);
		}
		
		ArrayList<Integer> result = new ArrayList<>(map.keySet());
//		System.out.println(map);
		// 내림차순
//		Collections.sort(result, (o1, o2) -> (map.get(o2).compareTo(map.get(o1))));
		
		
		return result;
    }
}
