package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Q4_W2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		int [][] board = new int[5][5];
		int [][] board = {{0,0,0,0,0}
						 ,{0,0,1,0,3}
						 ,{0,2,5,0,1}
						 ,{4,2,4,4,2}
						 ,{3,5,1,3,1}};
		int [] moves = {1,5,3,5,1,2,1,4};
		Random r = new Random();
		
//		for(int i = 0; i < 5; i++) {
//			for(int j = 0; j < 5; j++) {
//				board[i][j] = r.nextInt(6); 
//			}
//		}
		for(int i = 0; i < board.length * board[0].length; i++) {
			int row = i / board[0].length; //행
			int column = i % board[0].length; //열
//			if (row == 1)
				System.out.print("["+board[row][column]+"("+row+","+column+")]");
			
			if(column == board[0].length-1) {
				System.out.println();
			}
		}
		
		System.out.println();
		System.out.println(solution(board, moves));
		
//		System.out.println(solution(board, moves));
	}
	
	public static int solution(int[][] board, int[] moves) {
		int answer = 0;
		int k = 0;
		ArrayList<Integer> tmp = new ArrayList<Integer>();
		
		
		for(int i = 0; i < moves.length; i++) {
			for(int j = 0; j < board[0].length; j++) {
				int row = moves[i]-1;
				
				if (board[j][row] > 0) {
					tmp.add(board[j][row]);
					board[j][row] = 0;
					
					if(k>0 && tmp.get(k)==tmp.get(k-1)) {
						tmp.remove(k);
						tmp.remove(k-1);
						k -= 2;
						answer += 2;
					}
					k++;
					break;
				}
			}
		}
		
		return answer;
	}

}
