package test;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class Q2_W2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] numbers = {2,1,3,4,1};
		System.out.println(Arrays.toString(solution(numbers)));
	}

	public static int[] solution(int[] numbers) {
        int[] answer = {};
		HashSet<Integer> k = new HashSet<>();
		
		for(int i = 0; i < numbers.length; i++) {
			for(int j = 0; j < numbers.length; j++) {
				if(i != j) {
					int tmp = numbers[i] + numbers[j];
					if(tmp>0) {
						k.add(tmp);
					}
				}
			}
		}
		answer = new int[k.size()];
		
		Iterator it1 = k.iterator();
		
		int j = 0;
		while(it1.hasNext()) {
			answer[j] = (int) it1.next();
			j++;
		}
		
		// Arrays.sort(answer);
		// System.out.println(Arrays.toString(answer));
        Arrays.sort(answer);
        return answer;
    }
}
