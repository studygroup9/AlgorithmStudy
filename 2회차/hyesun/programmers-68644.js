// 두 개 뽑아서 더하기

function solution(numbers) {
  let answer = [];

  for (let i = 0; i <= numbers.length; i++) {
    for (let j = i + 1; j < numbers.length; j++) {
      let sum = numbers[i] + numbers[j];
      answer.push(sum);
    }
  }

  return [...new Set(answer)].sort((a, b) => a - b);
}

/**
테스트 1 〉	통과 (0.07ms, 30.1MB)
테스트 2 〉	통과 (0.10ms, 30.2MB)
테스트 3 〉	통과 (0.08ms, 30.1MB)
테스트 4 〉	통과 (0.06ms, 30.1MB)
테스트 5 〉	통과 (0.11ms, 29.8MB)
테스트 6 〉	통과 (0.17ms, 30.2MB)
테스트 7 〉	통과 (0.80ms, 30.2MB)
테스트 8 〉	통과 (0.78ms, 30.4MB)
테스트 9 〉	통과 (0.70ms, 30.1MB)
 */
