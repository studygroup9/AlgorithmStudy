package test;

public class W22_Q1 {

	//	Same Tree
		
	//	두 이진 트리 p및 의 루트가 주어지면 q동일한지 여부를 확인하는 함수를  작성하십시오.
	//	두 개의 이진 트리가 구조적으로 동일하고 노드가 동일한 값을 갖는 경우 동일한 것으로 간주됩니다.
	//	입력: p = [1,2,3], q = [1,2,3]
	//	출력: true
	//	입력: p = [1,2], q = [1,null,2]
	//	출력: false
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public boolean isSameTree(TreeNode p, TreeNode q) {
        boolean b = true;
        if(q != null && p == null || q == null && p != null) return false;
        
        
        if(p != null && q != null){
            if(p.val != q.val) b = false;    
            b = (isSameTree(p.left, q.left) && isSameTree(p.right, q.right)) && b;
        }
        
        return b;
    }

	 public class TreeNode {
	     int val;
	     TreeNode left;
	     TreeNode right;
	     TreeNode() {}
	     TreeNode(int val) { this.val = val; }
	     TreeNode(int val, TreeNode left, TreeNode right) {
	         this.val = val;
	         this.left = left;
	         this.right = right;
	     }
	 }
}
