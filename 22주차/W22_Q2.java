package test;

public class W22_Q2 {

	//	3진법 뒤집기
	//	문제 설명
	//	자연수 n이 매개변수로 주어집니다. n을 3진법 상에서 앞뒤로 뒤집은 후, 이를 다시 10진법으로 표현한 수를 return 하도록 solution 함수를 완성해주세요.
	
	public static void main(String[] args) {
		// TODO Auto-gene rated method stub
		solution(45);
	}
	public static int solution (int n) {
		String str = "";
			
		while(n > 0) {
			str = (n % 3) + str;
			n /= 3;
		}
		
		StringBuffer sb = new StringBuffer(str);
		str = sb.reverse().toString();
		
		n = Integer.parseInt(str, 3);
		
		return n;
		
	}
}
