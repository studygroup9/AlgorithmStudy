# **9주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [약수의 개수와 덧셈 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/77884)

2. [키패드 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/67256)

3. [오픈채팅방 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42888)

4. [디스크 컨트롤러 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42627)

2,3 레벨은 8주차와 동일합니다~

화이팅  :fire: 👍:construction_worker: 👍 :fire: