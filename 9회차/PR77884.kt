
class PR77884 {
    fun solution(left: Int, right: Int): Int {
        var answer: Int = 0
        
        
        for(i in left .. right){
            if(getFactorNum(i)%2 ==0){
                answer += i
            } else {
                answer -= i
            }
        }
        
        return answer
    
    
    }
    
    fun getFactorNum(num: Int): Int{
        var result = 0; 
        for(i in 1 .. num){
            if(num%i == 0){
                result++
            }
        }
        
        return result
    }
    
}
