class Solution {
    fun solution(record: Array<String>): Array<String> {
        var answer = arrayOf<String>()
        val idRecord = hashMapOf<String,String>() // uid, nick
        val actionRecord = mutableListOf<Pair<String,String>>() // action, uid 0: leave, 1: enter, 2: change

        record.forEach {
            val element = it.split(" ")
//            print(element)
            val action:String = element[0]
            val uid = element[1]
            val nick = if(action.equals("Leave")){
                ""
            } else{
                element[2]
            }


            if(action.equals("Change")){
                idRecord.replace(uid,nick)
             } else if(action.equals("Enter")){
                idRecord.set(uid,nick)
                actionRecord.add(Pair(uid,action))
            } else {
                actionRecord.add(Pair(uid,action))
            }

        }

        actionRecord.forEach { value ->

            val uid = value.first
            val action = value.second

            val nick = idRecord.get(uid)

            val actionStr = when(action) {
                "Enter"  -> "님이 들어왔습니다."
                "Leave"  -> "님이 나갔습니다."
                else -> ""
            }
            answer += nick + actionStr


        }

        return answer
    }
}
