# **10주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [문자열 다루기 기본 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12918)

2. [키패드 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/67256)

3. [가장 큰 수 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42746)

4. [디스크 컨트롤러 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42627)

키패드,디스크 컨트롤러는 8,9 주차와 동일합니다~

화이팅  :fire: 👍:construction_worker: 👍 :fire:
