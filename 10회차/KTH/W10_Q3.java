package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
  
public class W10_Q3 {

	/*
	 * 가장 큰 수
	 * sss
	 * 0 또는 양의 정수가 주어졌을 때, 정수를 이어 붙여 만들 수 있는 가장 큰 수를 알아내 주세요.
	 * 예를 들어, 주어진 정수가 [6, 10, 2]라면 [6102, 6210, 1062, 1026, 2610, 2106]를 만들 수 있고, 이중 가장 큰 수는 6210입니다.
	 * 0 또는 양의 정수가 담긴 배열 numbers가 매개변수로 주어질 때, 순서를 재배치하여 만들 수 있는 가장 큰 수를 문자열로 바꾸어 return 하도록 solution 함수를 작성해주세요.
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub  
		
		System.out.println(solution(new int[] {3, 30, 34, 5, 9}));
		
	}
	
	public static String solution(int[] numbers) {
        String answer = "";

        ArrayList<String> list = new ArrayList<>();

        for(int i = 0; i < numbers.length; i++){
            list.add(String.valueOf(numbers[i]));
        }


        Comparator<? super String> comparator = (o1, o2) -> {
            String q1 = o1.toString() + o2.toString();
            String q2 = o2.toString() + o1.toString();


            Long s1 = Long.parseLong(q1);
            Long s2 = Long.parseLong(q2);

            return s1 > s2 ? -1 : s1 == s2 ? 0 : 1 ;
        };


        Collections.sort(list, comparator);

        if(list.get(0).equals("0")){
            answer  = "0";
            return "0";
        } else {
            for(String str : list){
                answer += str;
            }
            return answer;
        }

    }

}

