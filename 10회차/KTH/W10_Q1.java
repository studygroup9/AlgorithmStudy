package test;

public class W10_Q1 {

	/*
	 * 문자열 다루기 기본
	 * 
	 * 문자열 s의 길이가 4 혹은 6이고, 숫자로만 구성돼있는지 확인해주는 함수, solution을 완성하세요. 
	 * 예를 들어 s가 "a234"이면 False를 리턴하고 "1234"라면 True를 리턴하면 됩니다.
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(solution("a234"));
		System.out.println(solution("a2_/d.f/df.234"));
		System.out.println(solution("as02fdfd34"));
		System.out.println(solution("a2';fls'df445er'we34"));
		System.out.println(solution("a2xcv;lbdf.,gma./a3-e0434"));
		
	}
	
	public static boolean solution(String s) {
        boolean answer = true;
        
        String s1 = s.replaceAll("[^\\d]", "");
        
        answer = ((s.length() == 4 || s.length() == 6) && (s1.length() == 4 || s1.length() == 6)) ? true : false;
            
        System.out.print(s1.length() + " ");
        return answer;
    }

}

