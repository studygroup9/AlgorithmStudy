//문제 : 문자열다루기 기본
//문자열 s의 길이가 4 혹은 6이고, 숫자로만 구성돼있는지 확인해주는 함수,
// solution을 완성하세요. 예를 들어 s가 "a234"이면 False를 리턴하고 "1234"라면 True를 리턴하면 됩니다.
#include <string>
#include <vector>

using namespace std;

bool solution(string s) {
    bool answer = true;
    if (s.length() != 4 && s.length() != 6)
        return false;
    
    for(int i=0 ; i < s.length() ; i++ )
    {
        // 0~9 문자열일때 true 문자 false or 0
        if (isdigit(s[i]) == false)
        {   
            return false;
            break;   
        }
         
    }
        
    return answer;
}