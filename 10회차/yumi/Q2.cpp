//문제 : 가장 큰 수 
//0 또는 양의 정수가 담긴 배열 numbers가 매개변수로 주어질 때, 
//순서를 재배치하여 만들 수 있는 가장 큰 수를 문자열로 바꾸어 return 
//하도록 solution 함수를 작성해주세요.

#include <string>
#include <vector>
#include<iostream>
#include<algorithm>
using namespace std;
 
//큰수를 반환하는 함수
bool cmp(string a, string b) {
    return a + b > b + a;
}
 
string solution(vector<int> numbers) {
    int Size = numbers.size();
    vector <string> str;
    string answer;
    for (int i = 0; i < Size; i++)
        str.push_back(to_string(numbers[i]));
    
    sort(str.begin(), str.end(),cmp);
    // 배열이 모두 0 일경우 예외처리
    if (str[0] == "0") return "0";
    
    for (int i = 0; i < Size; i++) {
            answer += str[i];
    }
    return answer;
}