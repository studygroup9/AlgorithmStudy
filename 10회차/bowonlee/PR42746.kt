package week10

class PR42746 {
    fun solution(numbers: IntArray): String {
        var answer = ""

        answer = numbers.map { it.toString() }.sortedWith(Comparator { a, b ->

            if((a+b) == (b+a)){
                0
            }
            else if((a+b) < (b+a)){
                1
            } else {
                -1
            }
        }).joinToString ( "" )

        if(answer.first() == '0'){
            return "0"
        }
        return answer
    }
}

// 무조건 앞자리가 큰 순서대로 돌린다.

fun main(){

    val sol = PR42746()
    println(sol.solution(intArrayOf(6,10,2)))
    println(sol.solution(intArrayOf(40,403)))
    println()
    println(sol.solution(intArrayOf(10, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)))
    println(sol.solution(intArrayOf(3, 30, 34, 5, 9)))
    println(sol.solution(intArrayOf(838,38,83)))
}
/**
 *
 * 테스트 1 〉	통과 (293.57ms, 98.6MB)
테스트 2 〉	통과 (190.81ms, 85.7MB)
테스트 3 〉	통과 (314.42ms, 107MB)
테스트 4 〉	통과 (58.15ms, 69.3MB)
테스트 5 〉	통과 (284.66ms, 87.1MB)
테스트 6 〉	통과 (244.80ms, 86.5MB)
테스트 7 〉	통과 (25.95ms, 58.8MB)
테스트 8 〉	통과 (26.52ms, 59.6MB)
테스트 9 〉	통과 (29.55ms, 59.6MB)
테스트 10 〉	통과 (29.12ms, 60MB)
테스트 11 〉	통과 (28.24ms, 58.8MB)
 *
 * */