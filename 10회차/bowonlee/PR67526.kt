package week10

import kotlin.math.abs

/**
 * 키패드
 * */
class PR67256 {
    fun solution(numbers: IntArray, hand: String): String {
        var answer = ""
        var left = 10
        var right = 11
        var hashMap = hashMapOf<Int, Pair<Int,Int>>()

        hashMap.put(1, Pair(0,0))
        hashMap.put(2, Pair(0,1))
        hashMap.put(3, Pair(0,2))
        hashMap.put(4, Pair(1,0))
        hashMap.put(5, Pair(1,1))
        hashMap.put(6, Pair(1,2))
        hashMap.put(7, Pair(2,0))
        hashMap.put(8, Pair(2,1))
        hashMap.put(9, Pair(2,2))
        hashMap.put(0, Pair(3,1))
        hashMap.put(10, Pair(3,0))
        hashMap.put(11, Pair(3,2))
        hashMap.put(100, Pair(5,5))




        numbers.forEach {

            when(it){
                1,4,7 -> {
                    answer +='L'
                    left = it
                }
                3,6,9 -> {
                    answer +='R'
                    right = it
                }
                2,5,8,0 -> {
                    val current = hashMap.get(it)!!
                    val leftPos = hashMap.get(left)!!
                    val rightPos = hashMap.get(right)!!

                    if(leftPos.minus(current) == rightPos.minus(current) ){
                        if(hand == "left"){
                            answer += 'L'
                            left = it
                        } else {
                            answer += 'R'
                            right = it
                        }
                    } else if(leftPos.minus(current) < rightPos.minus(current)){
                        answer += 'L'
                        left = it

                    } else{
                        answer += 'R'
                        right = it

                    }


                }
            }

        }
        return answer
    }

    operator fun Pair<Int,Int>.minus(b: Pair<Int,Int>): Int{

        return abs((this.first - b.first)) + abs((this.second - b.second))
    }

}

fun main(){

    val sol = PR67256()
    println(sol.solution(intArrayOf(7, 0, 8, 2, 8, 3, 1, 5, 7, 6, 2),"left"))

}

/**
 * 테스트 1 〉	통과 (0.42ms, 55.3MB)
테스트 2 〉	통과 (0.56ms, 54.8MB)
테스트 3 〉	통과 (0.53ms, 54.9MB)
테스트 4 〉	통과 (0.43ms, 54.7MB)
테스트 5 〉	통과 (0.47ms, 54.7MB)
테스트 6 〉	통과 (0.54ms, 54.9MB)
테스트 7 〉	통과 (0.53ms, 54.8MB)
테스트 8 〉	통과 (0.56ms, 55.1MB)
테스트 9 〉	통과 (0.53ms, 54.7MB)
테스트 10 〉	통과 (0.54ms, 54.5MB)
테스트 11 〉	통과 (0.64ms, 54.4MB)
테스트 12 〉	통과 (0.55ms, 54.6MB)
테스트 13 〉	통과 (0.70ms, 54.6MB)
테스트 14 〉	통과 (1.09ms, 55.6MB)
테스트 15 〉	통과 (2.09ms, 55.5MB)
테스트 16 〉	통과 (2.05ms, 55.6MB)
테스트 17 〉	통과 (3.34ms, 57MB)
테스트 18 〉	통과 (3.70ms, 56.6MB)
테스트 19 〉	통과 (4.09ms, 57.2MB)
테스트 20 〉	통과 (4.05ms, 56.4MB)
 *
 * */