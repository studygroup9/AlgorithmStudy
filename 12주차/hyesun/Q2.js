// 자연수 뒤집어 배열로 만들기
// https://programmers.co.kr/learn/courses/30/lessons/12932

function solution(n) {
  let answer = String(n)
    .split('')
    .reduce((acc, value) => {
      acc.unshift(value * 1);
      return acc;
    }, []);

  return answer;
}

/**
테스트 1 〉	통과 (0.05ms, 30.1MB)
테스트 2 〉	통과 (0.07ms, 30MB)
테스트 3 〉	통과 (0.07ms, 30.2MB)
테스트 4 〉	통과 (0.07ms, 29.8MB)
테스트 5 〉	통과 (0.08ms, 30.1MB)
테스트 6 〉	통과 (0.06ms, 30.1MB)
테스트 7 〉	통과 (0.07ms, 30.1MB)
테스트 8 〉	통과 (0.05ms, 30.1MB)
테스트 9 〉	통과 (0.08ms, 29.9MB)
테스트 10 〉	통과 (0.07ms, 30.1MB)
테스트 11 〉	통과 (0.07ms, 30MB)
테스트 12 〉	통과 (0.07ms, 30MB)
테스트 13 〉	통과 (0.07ms, 30MB)
 */
