package week12

class PR12932 {
    fun solution(n: Long): IntArray {
        var answer = intArrayOf()

        return n.toString().reversed().map { it.toInt() }.toIntArray()
    }
}