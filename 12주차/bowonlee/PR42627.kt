package week12

class PR42627 {
    fun solution(jobs: Array<IntArray>): Int {
        var answer = 0
        var currentTime = 0;



        var jobList = jobs.toMutableList()
        var fillteredList = jobList.filter { it[0] >= currentTime }.sortedWith(compareBy( {it[0] }, {it[1]} ) )

        while (true){

            print(currentTime)

            fillteredList = jobList.filter { it[0] <= currentTime }.sortedWith(compareByDescending ( {it[1]} ) )

            if(fillteredList.isEmpty()){
                currentTime++
                continue
            }   

            var currnetJob = fillteredList.first()

            print(currnetJob.toList())

            currentTime = currnetJob[0]
            answer += currnetJob[1]

            jobList.remove(currnetJob)
            jobList = jobList.map {
                intArrayOf(it[0], it[1] + currentTime)
            }.toMutableList()

            if(jobList.isEmpty()){
                return answer/jobs.size
            }

        }

    }
}


fun main(){

    val sol = PR42627()
    println(sol.solution( arrayOf(intArrayOf(0,3), intArrayOf(1,9), intArrayOf(2,6)) ))
}

/**
 *
 * 1. 첫번째 값 (시간)을 기준으로 가장 짧은 시간이 걸리는 인자를 실행
 * 2. 걸린 시간은 커런트 타임에 추가
 * 3. 남은 인자의 시간에 커런트 타임과 같거나 짧은 숫자들 모두 더함
 * 4. 그중 가장 짧은 숫자 더함.
 * 5. 인자가 남아 있다면 2 추가
 *
 * 위의 코드대로 하면.... 작업이 끝나야 다음 작업을 받아버린다..
 * **/