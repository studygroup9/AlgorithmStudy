# **12주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [자릿수 더하기 Lv 1: Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12931)

2. [자연수 뒤집어 배열로 만들기 : Programmers Lv 1](https://programmers.co.kr/learn/courses/30/lessons/12932)

3. [튜플 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/64065)

4. [디스크 컨트롤러 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42627)

디스크 컨트롤러는 8,9,10,11 주차와 동일합니다~

화이팅  :fire: 👍:construction_worker: 👍 :fire: