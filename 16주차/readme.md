# **16주차 문제**

* 1레벨 2문제, 2레벨 1문제, 3레벨 1문제 입니다.


1. [roman-to-integer Lv 1: leetCode Easy](https://leetcode.com/problems/roman-to-integer/)

2. [two-sum : leetCode Easy](https://leetcode.com/problems/two-sum/)

3. [위장 : Programmers Lv2](https://programmers.co.kr/learn/courses/30/lessons/42578)

4. [N으로 표현 : Programmers Lv3](https://programmers.co.kr/learn/courses/30/lessons/42895)

Lv3는 15주차와 동일합니다.

화이팅  :fire: 👍:construction_worker: 👍 :fire:
