// Roman to Integer
// https://leetcode.com/problems/roman-to-integer/
// 276 ms	50 MB

/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function (s) {
  let answser = 0;
  const special = [
    { k: 'CM', v: 900 },
    { k: 'CD', v: 400 },
    { k: 'XC', v: 90 },
    { k: 'XL', v: 40 },
    { k: 'IX', v: 9 },
    { k: 'IV', v: 4 },
    { k: 'M', v: 1000 },
    { k: 'D', v: 500 },
    { k: 'C', v: 100 },
    { k: 'L', v: 50 },
    { k: 'X', v: 10 },
    { k: 'V', v: 5 },
    { k: 'I', v: 1 },
  ];

  for (let i = 0; i < special.length; i++) {
    while (1) {
      let place = s.indexOf(special[i].k);
      if (place === -1) {
        break;
      }

      answser += special[i].v;
      s = s.slice(0, place) + s.slice(place + special[i].k.length);
      if (s.length === 0) {
        return answser;
      }
    }
  }

  return answser;
};
