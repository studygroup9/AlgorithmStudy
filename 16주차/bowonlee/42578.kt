/** 위장 */

class Solution {
fun solution(clothes: Array<Array<String>>): Int {
        var answer = 1
        val hash = hashMapOf<String,Int>()

        clothes.forEach {
            if(hash.containsKey(it[1])){
                hash.set( it[1] , hash.get(it[1])!! + 1 )
            } else{
                hash.set(it[1], 1)
            }
        }

        hash.values.forEach {
            answer *= it + 1
        }

        return answer - 1
    }
}