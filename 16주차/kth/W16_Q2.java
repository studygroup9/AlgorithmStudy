package test;

import java.util.ArrayList;
import java.util.Collections;

public class W16_Q2 {

	/*
	 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

		Symbol       Value
		I             1
		V             5
		X             10
		L             50
		C             100
		D             500
		M             1000
		For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.
		
		Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
		
		I can be placed before V (5) and X (10) to make 4 and 9. 
		X can be placed before L (50) and C (100) to make 40 and 90. 
		C can be placed before D (500) and M (1000) to make 400 and 900.
		Given a roman numeral, convert it to an integer.
		
		Example 1:
		
		Input: s = "III"
		Output: 3
		Example 2:
		
		Input: s = "IV"
		Output: 4
		Example 3:
		
		Input: s = "IX"
		Output: 9
		Example 4:
		
		Input: s = "LVIII"
		Output: 58
		Explanation: L = 50, V= 5, III = 3.
		Example 5:
		
		Input: s = "MCMXCIV"
		Output: 1994
		Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
	 * 
	 * */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(solution("LVIII"));
		
	}
	
	public static int solution(String s) {
        char[] a = s.toCharArray();
		int result = 0;
		
        for(int i = 0; i < a.length; i++) {
//        	System.out.println(a[i]);
        	if(a[i] == 'I') {
        		if (a.length > i+1 && a[i+1] == 'V') {
        			++i;
        			result += 4;
        		} else if (a.length > i+1 && a[i+1] == 'X') {
        			++i;
    				result += 9;
        		}else {
        			result += 1;
        		}
        	} else if(a[i] == 'V') {
        		result += 5;
        	} else if(a[i] == 'X') {
        		if (a.length > i+1 && a[i+1] == 'L') {
        			++i;
        			result += 40;
        		} else if (a.length > i+1 && a[i+1] == 'C') {
        			++i;
        			result += 90;
        		} else {
        			result += 10;
        		}
        	} else if(a[i] == 'L') {
        		result += 50;
        	} else if(a[i] == 'C') {
        		if (a.length > i+1 && a[i+1] == 'D') {
        			++i;
        			result += 400;
        		} else if (a.length > i+1 && a[i+1] == 'M') {
        			++i;
        			result += 900;
        		} else {
        			result += 100;
        		}
        	} else if(a[i] == 'D') {
        		result += 500;
        	} else if(a[i] == 'M') {
        		result += 1000;
        	} else {
        		result += 0;
        	}
        }
       
        return result;
    }
	
}

